#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
  #include <avr/power.h>
#endif
#include "RT.h"
#include "tempcolor.h"

/////////////////////////////
// User Settings
const uint8_t period = 15;     // average period between drops
const uint8_t framerate = 30;  // fps
const int temperature = 5000;  // K
const double brightness = 0.1; // x100%
/////////////////////////////

// Hardware Settings
const uint8_t numStrings = 16;
const uint8_t numPixels = 16;
const uint8_t pin[] = {2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, A0, A1, A2, A3};

// Program Variables
Adafruit_NeoPixel string[numStrings];
bool rain[numStrings * numPixels];
uint32_t on = temp.color(temperature, brightness);
uint32_t off = 0;

void setup() {
  for(uint8_t stringNum = 0; stringNum < numStrings; stringNum++) {
    string[stringNum] = Adafruit_NeoPixel(numPixels, pin[stringNum], NEO_RGB + NEO_KHZ800);
    string[stringNum].begin();
    string[stringNum].show();
  }

  rt.init(framerate);
}

void loop() {
  for(uint8_t stringNum = 0; stringNum < numStrings; stringNum++) {
    updateRain(stringNum);
    updateLights(stringNum);
  }
  rt.cycle();
}

void updateRain(uint8_t stringNum) {
  // move raindrops down 1
  for(uint8_t pixelNum = numPixels - 1; pixelNum > 0; pixelNum--) {
    rain[stringNum * numPixels + pixelNum] = rain[stringNum * numPixels + pixelNum - 1];
  }
  
  if(random(1,period) == 1)
    rain[stringNum * numPixels] = 1;
  else
    rain[stringNum * numPixels] = 0;
}

void updateLights(uint8_t stringNum) {
  for(uint8_t pixelNum = 0; pixelNum < numPixels; pixelNum++) {
    if(rain[stringNum * numPixels + pixelNum])
      string[stringNum].setPixelColor(pixelNum, on);
    else
      string[stringNum].setPixelColor(pixelNum, off);
  }
  
  string[stringNum].show();
}
