#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
  #include <avr/power.h>
#endif

const int numStrings = 16;
const int numLights = 16;
const int Pin[numStrings] = {2,3,4,5,6,7,8,9,10,11,12,13,A0,A1,A2,A3};

Adafruit_NeoPixel string[numStrings];

const unsigned int loopPeriod=16667;  // µsec
unsigned int loopEnd=micros();
unsigned int timeLeft=0;

void setup()
{
  for(int i = 0; i < numStrings; i++)
  {
    string[i] = Adafruit_NeoPixel(numLights, Pin[i], NEO_GRB + NEO_KHZ800);
    string[i].begin();
    string[i].show();
  }
}

void loop()
{
  for(int i = 0; i < numStrings; i++)
  {
    for(int j = 0; j < numLights; j++)
    {
      string[i].setPixelColor(j, string[i].Color(255, 0, 0));
    }
    
    string[i].show();
  }
  
  realtimeDelay();
}

void realtimeDelay()
{
  loopEnd+=loopPeriod;
  timeLeft=loopEnd-micros();
  delayMicroseconds(timeLeft);
}

