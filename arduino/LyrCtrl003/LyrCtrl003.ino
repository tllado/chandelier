#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
  #include <avr/power.h>
#endif

//****************************************************************************
// User Settings

const uint8_t numStrings = 16;  // per row
const uint8_t numLights = 16; // per string
const uint8_t frameRate = 30; //fps

const uint8_t Pin[numStrings] = {2,3,4,5,6,7,8,9,10,11,12,13,A0,A1,A2,A3};
const uint8_t SSPin = A4;

//****************************************************************************
// Program Variables

const uint32_t loopPeriod = 1000000 / frameRate;
uint32_t loopEnd = micros();
uint32_t timeLeft = 0;

Adafruit_NeoPixel string[numStrings];
uint8_t duty[3] = {0};

//****************************************************************************
// Initialization

void setup() {
  for(uint8_t stringNum = 0; stringNum < numStrings; stringNum++) {
    string[stringNum] = Adafruit_NeoPixel(numLights, Pin[stringNum], NEO_GRB + NEO_KHZ800);
    string[stringNum].begin();
    string[stringNum].show();
  }
}

//****************************************************************************
// Main Loop

void loop() {
  if(Serial.available()) {
    for(uint8_t stringNum = 0; stringNum < numStrings; stringNum++) {
      updateString(stringNum);
    }
    
    framerateDelay();
  }
}

//****************************************************************************
// Functions

void updateString(uint8_t stringNum) {
  for(uint8_t light = 0; light < numLights; light++) {
    duty[1] = ;
    duty[2] = ;
    duty[3] = ;
    string[stringNum].setPixelColor(light, string[stringNum].Color(duty[1], duty[2], duty[3]));
  }
  
  string[stringNum].show();
}

void framerateDelay() {
  loopEnd+=loopPeriod;
  timeLeft=loopEnd-micros();
  delayMicroseconds(timeLeft);
}

