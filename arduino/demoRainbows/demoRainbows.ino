#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
  #include <avr/power.h>
#endif
#include "RT.h"

/////////////////////////////
// User Settings
const uint8_t framerate = 60;   // fps
const double brightness = 0.3;  // %
/////////////////////////////

// Hardware Settings
const uint8_t numStrings = 16;
const uint8_t numPixels = 16;
const uint8_t pin[] = {2, 3, 4, 5, 6, 7, 8, 9, 10, 16, 14, 15, A0, A1, A2, A3};

// Program Variables
Adafruit_NeoPixel string[numStrings];

void setup() {
  for(uint8_t stringNum = 0; stringNum < numStrings; stringNum++) {
    string[stringNum] = Adafruit_NeoPixel(numPixels, pin[stringNum], NEO_RGB + NEO_KHZ800);
    string[stringNum].begin();
    string[stringNum].show();
  }

  rt.init(framerate);
}

void loop() {
  rainbowCycle();
}

void rainbowCycle() {
  uint32_t color = 0;
  
  for(uint16_t colorNum = 0; colorNum < 256*5; colorNum++) {
    for(uint8_t pixelNum = 0; pixelNum < numPixels; pixelNum++) {
      color = wheel(((pixelNum * 256 / numPixels) + colorNum) & 255);
      
      for(uint8_t stringNum = 0; stringNum < numStrings; stringNum++)
        string[stringNum].setPixelColor(pixelNum, color);
    }
    
    for(uint8_t stringNum = 0; stringNum < numStrings; stringNum++)
      string[stringNum].show();
      rt.cycle();
  }
}

uint32_t wheel(uint8_t wheelPos) {
  wheelPos = 255 - wheelPos;
  if(wheelPos < 85) {
    return string[0].Color((255 - wheelPos * 3)/2*brightness, 0, (wheelPos * 3)/2*brightness);
  }
  if(wheelPos < 170) {
    wheelPos -= 85;
    return string[0].Color(0, (wheelPos * 3)/2*brightness, (255 - wheelPos * 3)/2*brightness);
  }
  wheelPos -= 170;
  return string[0].Color((wheelPos * 3)/2*brightness, (255 - wheelPos * 3)/2*brightness, 0);
}
