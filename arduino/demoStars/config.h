// config.h
// Contains all user settings required for normal use of Chandelier.
// Last modified 2016.05.26

// Copyright 2016, Travis Llado
// travis@travisllado.com

// This file is part of Chandelier, v0.7.
// 
// Chandelier is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or any later version.
// 
// Chandelier is distributed in the hope that it will be useful,  but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
// 
// You should have received a copy of the GNU General Public License along with
// Chandelier.  If not, see <http://www.gnu.org/licenses/>.

////////////////////////////////////////////////////////////////////////////////
// Visualization Settings

// System
#define MAX_BRIGHTNESS      90
#define START_BRIGHTNESS    25
#define BRIGHT_INCREMENT    10
// Stars
#define STAR_PERIOD 1
#define NUM_STARS   50
#define NUM_VARS    6
#define MIN_COLOR   80
#define MAX_COLOR   165
#define MIN_LENGTH  10
#define MAX_LENGTH  50
#define MIN_BRIGHT  60
#define MAX_BRIGHT  255

////////////////////////////////////////////////////////////////////////////////
// Hardware Settings

#define FRAMERATE           10          // Hz
#define BIT_FREQ            800000      // Hz
#define CLOCK_FREQ          80000000    // Hz
#define BITS_PER_LIGHT      24
#define LIGHTS_PER_STRING   16
#define NUM_STRINGS         16
#define BIT_PRIORITY        1
#define FRAME_PRIORITY      2
#define BIT_PERIOD          CLOCK_FREQ/BIT_FREQ
#define FRAME_PERIOD        CLOCK_FREQ/FRAMERATE
#define SIGNAL_LENGTH       LIGHTS_PER_STRING*BITS_PER_LIGHT
