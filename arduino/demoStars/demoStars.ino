#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
  #include <avr/power.h>
#endif
#include "config.h"
#include "RT.h"

// Hardware Settings
const uint8_t pin[NUM_STRINGS] = {2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, A0, A1, A2, A3};

// Program Variables
Adafruit_NeoPixel string[NUM_STRINGS];
uint8_t brightness = START_BRIGHTNESS;
uint32_t LED[NUM_STRINGS*LIGHTS_PER_STRING] = {0};

uint8_t R[256] = {
    255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
    255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
    255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
    255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
    255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
    255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
    255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
    255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
    255,253,251,249,246,244,242,241,239,237,235,233,231,230,228,226,
    225,223,222,220,218,217,216,214,213,212,210,209,208,206,205,204,
    203,202,201,200,199,198,197,196,195,194,193,192,191,191,190,189,
    188,187,187,186,185,185,184,183,183,182,181,181,180,180,179,179,
    178,178,177,177,176,176,175,175,174,174,173,173,172,172,172,171,
    171,170,170,170,169,169,168,168,168,167,167,167,166,166,166,166,
    165,165,165,164,164,164,164,163,163,163,162,162,162,162,162,161,
    161,161,161,160,160,160,160,160,159,159,159,159,159,158,158,158
};

uint8_t G[256] = {
     69, 71, 73, 76, 78, 80, 82, 83, 85, 87, 89, 91, 92, 94, 96, 97,
     99,100,102,103,105,107,108,110,111,113,114,116,117,118,120,121,
    123,124,126,127,129,130,132,133,135,136,138,139,141,142,144,146,
    147,149,150,152,153,155,156,158,159,161,162,164,165,167,168,170,
    171,173,174,176,177,179,180,182,183,185,186,187,189,190,192,193,
    194,196,197,199,200,201,203,204,205,207,208,209,210,212,213,214,
    215,217,218,219,220,221,223,224,225,226,227,228,230,231,232,233,
    234,235,236,237,238,239,240,241,242,243,244,245,246,247,248,249,
    250,248,247,246,245,243,242,241,240,239,238,236,235,234,233,232,
    231,230,229,228,227,226,225,225,224,223,222,221,220,220,219,218,
    217,217,216,215,214,214,213,212,212,211,211,210,210,209,208,208,
    207,207,206,206,205,205,204,204,204,203,203,202,202,201,201,201,
    200,200,199,199,199,198,198,198,197,197,197,196,196,196,195,195,
    195,195,194,194,194,193,193,193,193,192,192,192,192,191,191,191,
    191,191,190,190,190,190,190,189,189,189,189,189,189,188,188,188,
    188,188,188,188,187,187,187,187,187,187,187,186,186,186,186,186
};

uint8_t B[256] = {
      0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
      0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
      0,  0,  0,  0,  0,  0,  0,  0,  4,  8, 12, 16, 20, 24, 28, 32,
     35, 39, 42, 46, 49, 52, 55, 59, 62, 65, 68, 71, 74, 76, 79, 82,
     85, 88, 90, 93, 96, 98,101,104,107,109,112,115,117,120,123,125,
    128,131,133,136,139,142,144,147,150,153,155,158,161,164,166,169,
    172,175,177,180,183,186,188,191,194,196,199,202,204,207,210,212,
    215,217,220,223,225,228,230,233,235,237,240,242,245,247,250,252,
    255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
    255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
    255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
    255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
    255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
    255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
    255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
    255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255
};

////////////////////////////////////////////////////////////////////////////////
// tempColor()
// Accepts two values (for temperature and brightness) and returns the
// corresponding neopixel color code (uint32_t). temp=0 is full red (1000K),
// temp=128 is neutral (6600K), and temp=255 is full blue (40000K).

uint32_t tempColor(uint8_t temp, uint8_t bright) {
    uint32_t Red = R[temp]*bright/255;
    uint32_t Grn = G[temp]*bright/255;
    uint32_t Blu = B[temp]*bright/255;

    return Red << 16 | Grn <<  8 | Blu;
}

void updateStars(void) {
    static uint32_t starCounter = 0;
    
    if(starCounter == STAR_PERIOD) {
        starCounter = 0;
        static uint32_t star[NUM_STARS*NUM_VARS] = {0};
        const uint32_t stringNum = 0;
        const uint32_t lightNum = 1;
        const uint32_t length = 2;
        const uint32_t bright = 3;
        const uint32_t color = 4;
        const uint32_t current = 5;
        uint32_t thisBrightness;
        
        for(int i = 0; i < NUM_STARS; i++) {
            // reinitialize single star
            if(star[i*NUM_VARS + current] == 0) {
                LED[star[i*NUM_VARS + stringNum]*LIGHTS_PER_STRING + star[i*NUM_VARS + lightNum]] = 0;
                        
                star[i*NUM_VARS + stringNum] = random(255)%(NUM_STRINGS - 1);
                star[i*NUM_VARS + lightNum] = random(255)%(LIGHTS_PER_STRING - 1);
                star[i*NUM_VARS + length] = random(255)%(MAX_LENGTH - MIN_LENGTH) + MIN_LENGTH;
                star[i*NUM_VARS + bright] = random(255)%(MAX_BRIGHT - MIN_BRIGHT) + MIN_BRIGHT;
                star[i*NUM_VARS + color] = random(255)%255;
                star[i*NUM_VARS + current] = star[i*NUM_VARS + length];
            }

            // update star brightness
            if(star[i*NUM_VARS + current] > star[i*NUM_VARS + length]/2)
                thisBrightness = brightness * star[i*NUM_VARS + bright]/255 * (star[i*NUM_VARS + length] - star[i*NUM_VARS + current])/(star[i*NUM_VARS + length]/2);
            else
                thisBrightness = brightness * star[i*NUM_VARS + bright]/255 * star[i*NUM_VARS + current]/(star[i*NUM_VARS + length]/2);
            star[i*NUM_VARS + current]--;

            // write new brightness value to LED array
            LED[star[i*NUM_VARS + stringNum]*LIGHTS_PER_STRING + star[i*NUM_VARS + lightNum]] = tempColor(MIN_COLOR + (MAX_COLOR - MIN_COLOR)*star[i*NUM_VARS + color]/255, thisBrightness);
        }
    }
    starCounter++;
}

void initLights(void) {
    for(uint8_t stringNum = 0; stringNum < NUM_STRINGS; stringNum++) {
        string[stringNum] = Adafruit_NeoPixel(LIGHTS_PER_STRING, pin[stringNum], NEO_RGB + NEO_KHZ800);
        string[stringNum].begin();
        string[stringNum].show();
    }
}

void updateLights(void) {
    for(uint8_t stringNum = 0; stringNum < NUM_STRINGS; stringNum++) {
        for(uint8_t pixelNum = 0; pixelNum < LIGHTS_PER_STRING; pixelNum++)
            string[stringNum].setPixelColor(pixelNum, LED[stringNum * LIGHTS_PER_STRING + pixelNum]);
        string[stringNum].show();
    }
}

void setup(void) {
    initLights();
    rt.init(FRAMERATE);
}

void loop(void) {
    updateStars();
    updateLights();
    rt.cycle();
}
