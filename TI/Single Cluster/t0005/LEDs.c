// LEDs.c
// Travis Llado
// 2016.05.07

////////////////////////////////////////////////////////////////////////////////
// Dependencies

#include "LEDs.h"
#include "tm4c123gh6pm.h"

void DisableInterrupts(void);
void EnableInterrupts(void);
long StartCritical (void);
void EndCritical(long sr);
void WaitForInterrupt(void);

////////////////////////////////////////////////////////////////////////////////
// Global Variables

#define PF1 (*((volatile unsigned long *)0x40025008))
#define PF2 (*((volatile unsigned long *)0x40025010))
#define PF3 (*((volatile unsigned long *)0x40025020))

////////////////////////////////////////////////////////////////////////////////

void LEDs_Init(void) {
    DisableInterrupts();
        SYSCTL_RCGCGPIO_R |= 0x20;          // activate port F
        while((SYSCTL_PRGPIO_R&0x20) == 0) {}
                                            // allow time for clock to stabilize
        GPIO_PORTF_DIR_R |= 0x0E;           // make PF1-3 output
        GPIO_PORTF_AFSEL_R &= ~0x0E;        // disable alt funct on PF1-3
        GPIO_PORTF_DEN_R |= 0x0E;           // enable digital I/O on PF1-3
        GPIO_PORTF_PCTL_R &= ~0x0000FFF0;   // configure PF1-3 as GPIO
        GPIO_PORTF_AMSEL_R &= ~0x0E;        // disable analog functions on PF1-3
    EnableInterrupts();
}

////////////////////////////////////////////////////////////////////////////////

void redLEDOn(void) {
    PF1 = 0x02;
}

////////////////////////////////////////////////////////////////////////////////

void redLEDOff(void) {
    PF1 &= ~0x02;
}

////////////////////////////////////////////////////////////////////////////////

void redLEDToggle(void) {
    PF1 ^= 0x02;
}

////////////////////////////////////////////////////////////////////////////////

void greenLEDOn(void) {
    PF3 = 0x08;
}

////////////////////////////////////////////////////////////////////////////////

void greenLEDOff(void) {
    PF3 &= ~0x08;
}

////////////////////////////////////////////////////////////////////////////////

void greenLEDToggle(void) {
    PF3 ^= 0x08;
}

////////////////////////////////////////////////////////////////////////////////

void blueLEDOn(void) {
    PF2 = 0x04;
}

////////////////////////////////////////////////////////////////////////////////

void blueLEDOff(void) {
    PF2 &= ~0x04;
}

////////////////////////////////////////////////////////////////////////////////

void blueLEDToggle(void) {
    PF2 ^= 0x04;
}
