// Switches.c
// Travis Llado
// 2016.05.07

////////////////////////////////////////////////////////////////////////////////
// Dependencies

#include "Switches.h"
#include <stdint.h>
#include "tm4c123gh6pm.h"

void DisableInterrupts(void);
void EnableInterrupts(void);
long StartCritical (void);
void EndCritical(long sr);
void WaitForInterrupt(void);

////////////////////////////////////////////////////////////////////////////////
// Internal Prototypes

void SW1_Init(void);
void SW2_Init(void);

////////////////////////////////////////////////////////////////////////////////
// System Settings

#define SW1   0x10  // on the left side of the Launchpad board
#define SW2   0x01  // on the right side of the Launchpad board

////////////////////////////////////////////////////////////////////////////////
// SW1_Init()
// Initializes Switch 1.
// Input: none
// Output: none

void SW1_Init(void) {
    DisableInterrupts();
    SYSCTL_RCGCGPIO_R |= 0x20;              // activate clock for port F   
    while((SYSCTL_PRGPIO_R&0x20) == 0) {}   // wait for clock to stabilize
    GPIO_PORTF_DIR_R &= ~0x10;              // make PF4 in (built-in button)
    GPIO_PORTF_AFSEL_R &= ~0x10;            // disable alt funct on PF4
    GPIO_PORTF_DEN_R |= 0x10;               // enable digital I/O on PF4   
    GPIO_PORTF_PCTL_R &= ~0x000F0000;       // configure PF4 as GPIO
    GPIO_PORTF_AMSEL_R &= ~0x10;            // disable analog functions on PF4
    GPIO_PORTF_PUR_R |= 0x10;               // enable weak pull-up on PF4
    GPIO_PORTF_IS_R &= ~0x10;               // PF4 is edge-sensitive
    GPIO_PORTF_IBE_R &= ~0x10;              // PF4 is not both edges
    GPIO_PORTF_IEV_R &= ~0x10;              // PF4 falling edge event
    GPIO_PORTF_ICR_R = 0x10;                // clear flag4
    GPIO_PORTF_IM_R |= 0x10;                // arm interrupt on PF4 
    NVIC_PRI7_R &= 0xFF00FFFF;              // priority 0
    NVIC_PRI7_R |= 0x00;                    // priority 0
    NVIC_EN0_R = 0x40000000;                // enable interrupt 30 in NVIC
    EnableInterrupts();
}

////////////////////////////////////////////////////////////////////////////////
// SW2_Init()
// Initializes Switch 2.
// Input: none
// Output: none

void SW2_Init(void) {
    DisableInterrupts();
    SYSCTL_RCGCGPIO_R |= 0x20;              // activate clock for port F   
    while((SYSCTL_PRGPIO_R&0x20) == 0) {}   // wait for clock to stabilize
    GPIO_PORTF_LOCK_R = GPIO_LOCK_KEY;      // unlock GPIO Port F Commit reg
    GPIO_PORTF_CR_R |= (SW1|SW2);           // enable commit for PF4 and PF0
    GPIO_PORTF_DIR_R &= ~0x01;              // make PF0 in (built-in button)
    GPIO_PORTF_AFSEL_R &= ~0x01;            // disable alt funct on PF0
    GPIO_PORTF_DEN_R |= 0x01;               // enable digital I/O on PF0   
    GPIO_PORTF_PCTL_R &= ~0x0F;             // configure PF0 as GPIO
    GPIO_PORTF_AMSEL_R &= ~0x01;            // disable analog functions on PF0
    GPIO_PORTF_PUR_R |= 0x01;               // enable weak pull-up on PF0
    GPIO_PORTF_IS_R &= ~0x01;               // PF0 is edge-sensitive
    GPIO_PORTF_IBE_R &= ~0x01;              // PF0 is not both edges
    GPIO_PORTF_IEV_R &= ~0x01;              // PF0 falling edge event
    GPIO_PORTF_ICR_R = 0x01;                // clear flag0
    GPIO_PORTF_IM_R |= 0x01;                // arm interrupt on PF0 
    NVIC_PRI7_R &= 0xFF00FFFF;              // priority 0
    NVIC_PRI7_R |= 0x00;                    // priority 0
    NVIC_EN0_R = 0x40000000;                // enable interrupt 30 in NVIC
    DisableInterrupts();
}

////////////////////////////////////////////////////////////////////////////////
// Switches_Init()
// Initialized hardware and software associated with Launchpad's two onboard
//  switches
// Input: none
// Output: none

void Switches_Init(void) {
  SW1_Init();
  SW2_Init();
}
