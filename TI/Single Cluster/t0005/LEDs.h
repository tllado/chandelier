// LEDs.h
// Travis Llado
// 2016.05.07

////////////////////////////////////////////////////////////////////////////////
// External Prototypes

void blueLEDOn(void);
void blueLEDOff(void);
void blueLEDToggle(void);
void greenLEDOn(void);
void greenLEDOff(void);
void greenLEDToggle(void);
void LEDs_Init(void);
void redLEDOn(void);
void redLEDOff(void);
void redLEDToggle(void);
