// main.c
// Travis Llado
// 2016.05.22

////////////////////////////////////////////////////////////////////////////////
// Dependencies

#include <cstdlib>
#include <stdint.h>
#include "boardsetup.h"
#include "config.h"
#include "PLL.h"
#include "Switches.h"
#include "tempcolor.h"
#include "tm4c123gh6pm.h"
#include "LEDs.h"

////////////////////////////////////////////////////////////////////////////////
// Global Variables

const uint8_t numPrograms = 3;
uint8_t  programNum = 2;
uint8_t  brightness = START_BRIGHTNESS;
uint32_t LED[NUM_STRINGS][LIGHTS_PER_STRING] = {0};
uint32_t bits[SIGNAL_LENGTH] = {0};
uint32_t bitCount = 0;

////////////////////////////////////////////////////////////////////////////////
// Internal Prototypes

void GPIOPortF_Handler(void);
void Timer1A_Handler(void);
void Timer2A_Handler(void);
void transformLights(void);
void clearLEDs(void);
void updateLights(void);
uint8_t updateRain(void);
uint32_t rainbowHue(int hue);
uint8_t updateRainbow(void);
uint8_t updateStars(void);

////////////////////////////////////////////////////////////////////////////////
// GPIOPortF_Handler()
// Decides what action to take for any interrupt on Port F. This includes the
//  Launchpad's two onboard switches.

void GPIOPortF_Handler(void) {
    if(GPIO_PORTF_RIS_R & 0x10) {   // poll PF4(SW1)
        GPIO_PORTF_ICR_R = 0x10;    // acknowledge flag4
        brightness = (brightness + BRIGHT_INCREMENT)%MAX_BRIGHTNESS;
    }
    if(GPIO_PORTF_RIS_R & 0x01) {   // poll PF0(SW2)
        GPIO_PORTF_ICR_R = 0x01;    // acknowledge flag0
        programNum = (programNum + 1)%numPrograms;
        redLEDToggle();
    }
}

////////////////////////////////////////////////////////////////////////////////
// Starts sequence of visualization and LED update for a single frame.

void Timer1A_Handler(void) {
    static uint32_t lastProgram;
    uint32_t update = 0;

    TIMER1_ICR_R = 0x01;    // acknowledge timer1A timeout

    if(lastProgram != programNum)
        clearLEDs();
		lastProgram = programNum;
    
    switch(programNum) {
        case 0:
            update = updateRain();
            break;
        case 1:
            update = updateRainbow();
            break;
        case 2:
            update = updateStars();
            break;
    }

    if(update)
        updateLights();
}

////////////////////////////////////////////////////////////////////////////////
// Timer2A_Handler()
// Writes single bit to all LED strings

void Timer2A_Handler(void) {
    uint32_t volatile delay = 0;

    TIMER2_ICR_R = 0x01;                        // acknowledge timer2A timeout
    
    GPIO_PORTA_DATA_R = 0xFF;                   // write all PortA pins high
    delay=0;delay=0;delay=0;
    GPIO_PORTB_DATA_R = 0xFF;                   // write all PortB pins high
    GPIO_PORTA_DATA_R = bits[bitCount];         // write PortA to message bits
    GPIO_PORTB_DATA_R = bits[bitCount] >> 8;    // write PortB to message bits
    delay=0;delay=0;delay=0;delay=0;delay=0;    // delay remaining 1 time
    GPIO_PORTA_DATA_R = 0x00;                   // write all PortA pins low
    delay=0;delay=0;delay=0;
    GPIO_PORTB_DATA_R = 0x00;                   // write all PortB pins low

    bitCount--;
    if(bitCount == 0)                           // after entire message is sent
        TIMER2_CTL_R = 0x00;                    // disable Timer 2A
}

////////////////////////////////////////////////////////////////////////////////
// clearLEDs()
// Sets all LEDs to be off on next update.

void clearLEDs(void) {
    for(int i = 0; i < NUM_STRINGS; i++) {
        for(int j = 0; j < LIGHTS_PER_STRING; j++)
            LED[i][j] = 0;
    }
}

////////////////////////////////////////////////////////////////////////////////
// updateLights()
// Performs actions to commence writing of current contents of LED[][] to LEDs.

void updateLights(void) {
    transformLights();
    bitCount = SIGNAL_LENGTH - 1;       // reset message bit counter
    TIMER2_CTL_R = 0x01;                // enable Timer 2A
}

////////////////////////////////////////////////////////////////////////////////
// updateRain()
// Produces rain visualization. Raindrops fall vertically at user defined
// frequency, density, and speed.

uint32_t rainCounter = 0;

uint8_t updateRain(void) {
    
    if(rainCounter == RAIN_PERIOD) {
        rainCounter = 0;
        
        uint32_t onColor;
        onColor = tempColor(RAIN_COLOR, 1.0*brightness/255);

        for(int stringNum = 0; stringNum < NUM_STRINGS; stringNum++) {
            if(rand()%RAIN_PROB == 1)
                LED[stringNum][LIGHTS_PER_STRING - 1] = onColor;
            else
                LED[stringNum][LIGHTS_PER_STRING - 1] = 0;
                
            for(int lightNum = 0; lightNum < LIGHTS_PER_STRING - 1; lightNum++)
                LED[stringNum][lightNum] = LED[stringNum][lightNum + 1];
        }
				return 1;
    }
    rainCounter++;
		
		return 0;
}

////////////////////////////////////////////////////////////////////////////////
// rainbowHue()
// Create 24-bit color code from 8-bit hue

uint32_t rainbowHue(int hue) {
    hue = 255 - hue;
    if(hue < 85)
        return ((255 - hue*3)*brightness/256 << 16) + (hue*3)*brightness/256;
    if(hue < 170) {
        hue -= 85;
        return ((hue*3)*brightness/256 << 8) + (255 - hue*3)*brightness/256;
    }
    hue -= 170;
    return ((hue*3)*brightness/256 << 16) + ((255 - hue*3)*brightness/256 << 8);
}  

////////////////////////////////////////////////////////////////////////////////
// updateRainbow()
// Produces rainbow visualization. Rainbow cycles flow steadily downward.

uint8_t updateRainbow(void) {
    static uint8_t rainbowColor = 0;
    static uint8_t nextColor = 0;
    int thisColor[LIGHTS_PER_STRING] = {rainbowHue(rainbowColor)};
    
    for(int a = 1; a < LIGHTS_PER_STRING; a++) {
        nextColor = rainbowColor + a*LIGHTS_PER_STRING;
        thisColor[a] = rainbowHue(nextColor);
    }
    for(int i = 0; i < LIGHTS_PER_STRING; i++) {
        for(int j = 0; j < LIGHTS_PER_STRING; j++)
            LED[i][LIGHTS_PER_STRING - j] = thisColor[(LIGHTS_PER_STRING + i - j)%LIGHTS_PER_STRING];
    }
    rainbowColor++;
		
		return 1;
}

////////////////////////////////////////////////////////////////////////////////
// updateStars()
// Produces starfield visualization. Stars twinkle on and with random position,
//  brightness, and period.

uint8_t updateStars(void) {
    static uint32_t starCounter = 0;
    
    if(starCounter == STAR_PERIOD) {
        starCounter = 0;
        static uint32_t star[NUM_STARS][6] = {0};
		    const uint32_t stringNum = 0;
        const uint32_t lightNum = 1;
        const uint32_t length = 2;
        const uint32_t bright = 3;
        const uint32_t color = 4;
        const uint32_t current = 5;
        uint32_t thisBrightness;
        
        for(int i = 0; i < NUM_STARS; i++) {
            // reinitialize single star
            if(star[i][current] == 0) {
                LED[star[i][stringNum]][star[i][lightNum]] = 0;
                        
                star[i][stringNum] = rand()%(NUM_STRINGS - 1);
                star[i][lightNum] = rand()%(LIGHTS_PER_STRING - 1);
                star[i][length] = rand()%(MAX_LENGTH - MIN_LENGTH) + MIN_LENGTH;
                star[i][bright] = rand()%(MAX_BRIGHT - MIN_BRIGHT) + MIN_BRIGHT;
                star[i][color] = rand()%255;
                star[i][current] = star[i][length];
            }

            // update star brightness
            if(star[i][current] > star[i][length]/2)
                thisBrightness = brightness * star[i][bright]/255 *            \
                    (star[i][length] - star[i][current])/(star[i][length]/2);
            else
                thisBrightness = brightness * star[i][bright]/255 *            \
                    star[i][current]/(star[i][length]/2);
            star[i][current]--;

            // write new brightness value to LED array
            LED[star[i][stringNum]][star[i][lightNum]] =                       \
                tempColor(MIN_COLOR + (MAX_COLOR - MIN_COLOR)*                 \
                star[i][color]/255, 1.0*thisBrightness/255);
        }
				
				return 1;
    }

    starCounter++;
		
		return 0;
}

////////////////////////////////////////////////////////////////////////////////
// transformLights()
// Transforms 16x16 array of LED color values into 16x(24x16) array of GPIO bits

void transformLights(void) {
    int bitIndex = 0;
    for(int lightNum = 0; lightNum < LIGHTS_PER_STRING; lightNum++) {
        for(int bitNum = 0; bitNum < BITS_PER_LIGHT; bitNum++) {
            bitIndex = lightNum*BITS_PER_LIGHT + bitNum;
            bits[bitIndex] = 0;
            for(int stringNum = 0; stringNum < NUM_STRINGS; stringNum++) {
                bits[bitIndex] += ((LED[stringNum][lightNum] &
                    (1 << bitNum)) >> bitNum) << stringNum;
            }
        }
    }
}

////////////////////////////////////////////////////////////////////////////////
// main()
// Outermost structure of chandelier program. Initializes all hardware, then
// spins in a while loop forever as all further actions are performed by
// interrupt handlers.

int main(void) {
    PLL_Init();
    PortA_Init();
    PortB_Init();
    Switches_Init();
    Timer1_Init();
    Timer2_Init();
    LEDs_Init();
    
    while(1) {
        // NOTHING HERE!
        // Everything is done using timer interrupt handlers
    }
}
