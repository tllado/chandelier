// config.h
// Travis Llado
// 2016.05.22

////////////////////////////////////////////////////////////////////////////////
// User Settings

// Brightness Settings
#define MAX_BRIGHTNESS      90
#define START_BRIGHTNESS    25
#define BRIGHT_INCREMENT    10

// Rain Settings
#define RAIN_PERIOD 3
#define RAIN_COLOR  -0.1
#define RAIN_PROB   15

// Stars Settings
#define STAR_PERIOD 1
#define NUM_STARS   50
#define MIN_COLOR   -0.25
#define MAX_COLOR   0.2
#define MIN_LENGTH  20
#define MAX_LENGTH  200
#define MIN_BRIGHT  63
#define MAX_BRIGHT  255
