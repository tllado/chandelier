// tempcolor.h

#include <stdint.h>

uint32_t tempColor(double temperature, double brightness);
uint8_t R(double);
uint8_t G(double);
uint8_t B(double);
