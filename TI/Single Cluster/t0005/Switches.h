// Switches.h
// Travis Llado
// 2016.05.07

////////////////////////////////////////////////////////////////////////////////
// switchesInit()
// Initialized hardware and software associated with Launchpad's two onboard
//  switches
// Input: none
// Output: none

void Switches_Init(void);
