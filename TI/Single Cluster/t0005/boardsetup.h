// boardsetup.h
// Travis Llado
// 2016.05.16

////////////////////////////////////////////////////////////////////////////////
// External Prototypes

void PortA_Init(void);
void PortB_Init(void);
void Timer1_Init(void);
void Timer2_Init(void);

////////////////////////////////////////////////////////////////////////////////
// User Settings

#define FRAMERATE           60          // Hz
#define BIT_FREQ            800000      // Hz
#define CLOCK_FREQ          80000000    // Hz
#define BITS_PER_LIGHT      24
#define LIGHTS_PER_STRING   16
#define NUM_STRINGS         16
#define BIT_PRIORITY        1
#define FRAME_PRIORITY      2

#define BIT_PERIOD      CLOCK_FREQ/BIT_FREQ
#define FRAME_PERIOD    CLOCK_FREQ/FRAMERATE
#define SIGNAL_LENGTH   LIGHTS_PER_STRING*BITS_PER_LIGHT
