// main.c
// Main procedure for initialization and operation of Chandelier. Initializes
// all hardware and memory needed for normal operation, the commences indefinite
// execution of Chandelier visualizations.

// Chandelier t0007
// Copyright (c) 2016 Travis Llado, travis@travisllado.com
// Last modified 2016.12.26

// This file is part of Chandelier t0007.
// 
// Chandelier is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
// 
// Chandelier is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with
// Chandelier.  If not, see <http://www.gnu.org/licenses/>.

////////////////////////////////////////////////////////////////////////////////
// Dependencies

// hardware
#include <stdint.h>
#include "buttons.h"
#include "config.h"
#include "leds.h"
#include "lights.h"
#include "PLL.h"

// visualizations
#include "rain.h"
#include "rainbows.h"
#include "stars.h"

////////////////////////////////////////////////////////////////////////////////
// Global Variables

const uint8_t numPrograms = 3;
uint8_t programNum = 0;
uint8_t brightness = START_BRIGHTNESS;
uint32_t LED[NUM_STRINGS*LIGHTS_PER_STRING] = {0};

////////////////////////////////////////////////////////////////////////////////
// Internal Prototypes

void leftButtonTask(void);
void rightButtonTask(void);
void visualizations(void);
void wait(uint32_t length);

////////////////////////////////////////////////////////////////////////////////
// main()
// Outermost structure of chandelier program. Initializes all hardware, then
// spins in a while loop forever as all further actions are performed by
// interrupt handlers.

int main(void) {
    PLL_Init();
    buttonsInit(&leftButtonTask, &rightButtonTask);
    LEDsInit();
    lightsInit(&visualizations, LED);
    wait(CHARGE_TIME);
    lightsOn();
    
    while(1) {
        // NOTHING HERE!
        // Everything else is done using timer interrupt handlers
    }
}

////////////////////////////////////////////////////////////////////////////////
// leftButtonTask()
// Actions performed when left button is pressed

void leftButtonTask(void) {
    brightness = (brightness + BRIGHT_INCREMENT)%MAX_BRIGHTNESS;
    greenLEDToggle();
}

////////////////////////////////////////////////////////////////////////////////
// rightButtonTask()
// Actions performed when right button is pressed

void rightButtonTask(void) {
    programNum = (programNum + 1)%(numPrograms + 1);
    blueLEDToggle();
}

////////////////////////////////////////////////////////////////////////////////
// visualizations()
// Code executed once per frame period. All visualizations should be selected
// and called from here.

void visualizations(void) {
    static uint32_t lastProgram;
    
    if(lastProgram != programNum)
              // clear lights every time program is changed
        clearLights();
    else {
        switch(programNum) {
            case 0:
                updateStars(LED, brightness);
                break;
            case 1:
                updateRain(LED, brightness);
                break;
            case 2:
                updateRainbow(LED, brightness);
                break;
            case 3:
                // do nothing, LEDs are off
                break;
        }
    }
    
    lastProgram = programNum;
}

////////////////////////////////////////////////////////////////////////////////
// wait()
// Idles the CPU for desired time. Input desired time in milliseconds.

void wait(uint32_t length) {
    const uint32_t factor = 5400;   // converts ms to cycles
    uint32_t spin = 0;
    
    redLEDOn();
    for(uint32_t j = 0; j < length*factor; j++)
        spin++;
    redLEDOff();
}
