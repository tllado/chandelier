// rain.h
// Rain visualization for Chandelier

// Chandelier t0007
// Copyright (c) 2016 Travis Llado, travis@travisllado.com
// Last modified 2016.12.26

// This file is part of Chandelier t0007.
// 
// Chandelier is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
// 
// Chandelier is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with
// Chandelier.  If not, see <http://www.gnu.org/licenses/>.

////////////////////////////////////////////////////////////////////////////////
// User Settings

#define RAIN_PERIOD 3   // 1/update_freq
#define RAIN_COLOR  250 // 0-255
#define RAIN_PROB   15  // 1/avg_freq of generating new rain drops

////////////////////////////////////////////////////////////////////////////////
// Exported Functions

void updateRain(uint32_t *LEDs, uint8_t brightness);
