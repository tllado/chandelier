// config.h
// Contains all user settings required for normal use of Chandelier.

// Chandelier t0007
// Copyright (c) 2016 Travis Llado, travis@travisllado.com
// Last modified 2016.12.26

// This file is part of Chandelier t0007.
// 
// Chandelier is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
// 
// Chandelier is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with
// Chandelier.  If not, see <http://www.gnu.org/licenses/>.

////////////////////////////////////////////////////////////////////////////////
// Visualization Settings

#define MAX_BRIGHTNESS      90  // 0-255
#define START_BRIGHTNESS    25  // 0-255
#define BRIGHT_INCREMENT    10  // 0-255

////////////////////////////////////////////////////////////////////////////////
// Hardware Settings

#define CHARGE_TIME         1000        // ms
#define FRAMERATE           60          // Hz
#define BIT_FREQ            800000      // Hz
#define CLOCK_FREQ          80000000    // Hz
#define BITS_PER_LIGHT      24
#define LIGHTS_PER_STRING   16
#define NUM_STRINGS         16
#define BIT_PRIORITY        1
#define FRAME_PRIORITY      2

#define BIT_PERIOD          CLOCK_FREQ/BIT_FREQ
#define FRAME_PERIOD        CLOCK_FREQ/FRAMERATE
#define SIGNAL_LENGTH       LIGHTS_PER_STRING*BITS_PER_LIGHT
