// leds.h
// Initializes and handles LEDs onboard TI TM4C MCU.

// Chandelier t0007
// Copyright (c) 2016 Travis Llado, travis@travisllado.com
// Last modified 2016.12.26

// This file is part of Chandelier t0007.
// 
// Chandelier is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
// 
// Chandelier is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with
// Chandelier.  If not, see <http://www.gnu.org/licenses/>.

////////////////////////////////////////////////////////////////////////////////
// External Prototypes

void LEDsInit(void);
void redLEDOn(void);
void redLEDOff(void);
void redLEDToggle(void);
void greenLEDOn(void);
void greenLEDOff(void);
void greenLEDToggle(void);
void blueLEDOn(void);
void blueLEDOff(void);
void blueLEDToggle(void);
