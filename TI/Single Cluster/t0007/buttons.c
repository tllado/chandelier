// buttons.c
// Initializes and handles buttons onboard TI TM4C MCU.

// Chandelier t0007
// Copyright (c) 2016 Travis Llado, travis@travisllado.com
// Last modified 2016.12.26

// This file is part of Chandelier t0007.
// 
// Chandelier is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
// 
// Chandelier is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with
// Chandelier.  If not, see <http://www.gnu.org/licenses/>.

////////////////////////////////////////////////////////////////////////////////
// Dependencies

#include <stdint.h>
#include "buttons.h"

void DisableInterrupts(void);
void EnableInterrupts(void);

////////////////////////////////////////////////////////////////////////////////
// Global Variables

#define SW1                 0x10
#define SW2                 0x01

#define GPIO_PORTF_RIS_R        (*((volatile uint32_t *)0x40025414))
#define GPIO_PORTF_ICR_R        (*((volatile uint32_t *)0x4002541C))
#define SYSCTL_RCGCGPIO_R       (*((volatile uint32_t *)0x400FE608))
#define SYSCTL_RCGCGPIO_R5      0x00000020  // GPIO Port F Run Mode Clock
                                            // Gating Control
#define SYSCTL_PRGPIO_R         (*((volatile uint32_t *)0x400FEA08))
#define SYSCTL_PRGPIO_R5        0x00000020  // GPIO Port F Peripheral Ready
#define GPIO_PORTF_LOCK_R       (*((volatile uint32_t *)0x40025520))
#define GPIO_LOCK_KEY           0x4C4F434B  // Unlocks the GPIO_CR register
#define GPIO_PORTF_CR_R         (*((volatile uint32_t *)0x40025524))
#define GPIO_PORTF_DIR_R        (*((volatile uint32_t *)0x40025400))
#define GPIO_PORTF_AFSEL_R      (*((volatile uint32_t *)0x40025420))
#define GPIO_PORTF_DEN_R        (*((volatile uint32_t *)0x4002551C))
#define GPIO_PORTF_PCTL_R       (*((volatile uint32_t *)0x4002552C))
#define GPIO_PORTF_AMSEL_R      (*((volatile uint32_t *)0x40025528))
#define GPIO_PORTF_PUR_R        (*((volatile uint32_t *)0x40025510))
#define GPIO_PORTF_IS_R         (*((volatile uint32_t *)0x40025404))
#define GPIO_PORTF_IBE_R        (*((volatile uint32_t *)0x40025408))
#define GPIO_PORTF_IEV_R        (*((volatile uint32_t *)0x4002540C))
#define GPIO_PORTF_ICR_R        (*((volatile uint32_t *)0x4002541C))
#define GPIO_PORTF_IM_R         (*((volatile uint32_t *)0x40025410))
#define NVIC_PRI7_R             (*((volatile uint32_t *)0xE000E41C))
#define NVIC_EN0_R              (*((volatile uint32_t *)0xE000E100))

void (*SW1Task)(void);
void (*SW2Task)(void);

////////////////////////////////////////////////////////////////////////////////
// Internal Prototypes

void buttonsInit(void (*leftTask)(void), void (*rightTask)(void));
void debounce(void);
void GPIOPortF_Handler(void);
void SW12_Init(void);

////////////////////////////////////////////////////////////////////////////////
// buttonsInit()
// Initializes hardware and software associated with Launchpad's two onboard
// switches. Takes input of two function pointers that are referenced when
// GPIO Port F interrupt handler is executed.

void buttonsInit(void (*leftTask)(void), void (*rightTask)(void)) {
    SW12_Init();
    SW1Task = leftTask;
    SW2Task = rightTask;
}

////////////////////////////////////////////////////////////////////////////////
// GPIOPortF_Handler()
// A very primitive debounce function. Just wastes some time before clearing
// button interrupt.

void debounce(void) {
    volatile uint32_t delay = 0;
    const uint32_t length = 1000000;
    
    for(uint32_t i = 0; i < length; i++)
        delay = 0;
}

////////////////////////////////////////////////////////////////////////////////
// GPIOPortF_Handler()
// Decides what action to take for any interrupt on Port F. This includes the
// Launchpad's two onboard switches.

void GPIOPortF_Handler(void) {
    if(GPIO_PORTF_RIS_R & SW1) {   // poll PF4(SW1)
        debounce();
        GPIO_PORTF_ICR_R = SW1;    // acknowledge flag4
        SW1Task();
    }
    if(GPIO_PORTF_RIS_R & SW2) {   // poll PF0(SW2)
        debounce();
        GPIO_PORTF_ICR_R = SW2;    // acknowledge flag0
        SW2Task();
    }
}

////////////////////////////////////////////////////////////////////////////////
// SW12_Init()
// Initializes Switches 1 and 2
// Input: none
// Output: none

void SW12_Init() {
    DisableInterrupts();
    SYSCTL_RCGCGPIO_R |= SYSCTL_RCGCGPIO_R5;
                                        // activate clock for port F
    while((SYSCTL_PRGPIO_R & SYSCTL_PRGPIO_R5) == 0) {}
                                        // wait for clock to stabilize
    GPIO_PORTF_LOCK_R = GPIO_LOCK_KEY;  // unlock GPIO Port F Commit reg
    GPIO_PORTF_CR_R |= (SW1|SW2);       // enable commit for PF4 and PF0
    GPIO_PORTF_DIR_R &= ~(SW1|SW2);     // make PF4 in (built-in button)
    GPIO_PORTF_AFSEL_R &= ~(SW1|SW2);   // disable alt funct on PF4
    GPIO_PORTF_DEN_R |= (SW1|SW2);      // enable digital I/O on PF4
    GPIO_PORTF_PCTL_R &= ~0x000F000F;   // configure PF4 as GPIO
    GPIO_PORTF_AMSEL_R &= ~(SW1|SW2);   // disable analog functions on PF4
    GPIO_PORTF_PUR_R |= (SW1|SW2);      // enable weak pull-up on PF4
    GPIO_PORTF_IS_R &= ~(SW1|SW2);      // PF4 is edge-sensitive
    GPIO_PORTF_IBE_R &= ~(SW1|SW2);     // PF4 is not both edges
    GPIO_PORTF_IEV_R &= ~(SW1|SW2);     // PF4 falling edge event
    GPIO_PORTF_ICR_R = (SW1|SW2);       // clear flag4
    GPIO_PORTF_IM_R |= (SW1|SW2);       // arm interrupt on PF4
    NVIC_PRI7_R &= 0xFF00FFFF;          // priority 0
    NVIC_PRI7_R |= 0x00;                // priority 0
    NVIC_EN0_R = 0x40000000;            // enable interrupt 30 in NVIC
    EnableInterrupts();
}
