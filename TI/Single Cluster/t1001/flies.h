// Flies.h
// Fireflies visualization for Chandelier.

// This file is part of Chandelier t0008
// Travis Llado, travis@travisllado.com
// Last modified 2016.12.15

////////////////////////////////////////////////////////////////////////////////
// User Settings

#define NUM_FLIES	5		// # per cluster
#define SPEED		768		// 1/256 lights per seconds
#define MIN_LENGTH	256		// 1/256 seconds
#define MAX_LENGTH	1280	// 1/256 seconds
#define COLOR 		100		// 0-255, color temperature
#define BRIGHTNESS 	255 	// 0-255
#define RANDOM_GAIN	25		// 255 scale
#define WALL_GAIN	51		// 255 scale

////////////////////////////////////////////////////////////////////////////////
// Exported Functions

void updateFlies(uint32_t *LED, uint8_t brightness);
