Chandelier Single Cluster
test build 1001

2016.12.28
Released v1.0, so this will be first test build for v2. Since v2 will combine multiple clusters, we need multiple software components. We should end up having three in total: one for Tiva board that controls single cluster, one for chandelier control PC that sends UART commands to 16 Tiva boards, and one for remote control that sends UDP commands to chandelier control PC. Could also create web server for control and adjustment, access from any wifi connected device.
Anyway, need to work out hardware bugs mentioned previously, ensure all 16 strings are individually controllable. Then have to see if we can do full bandwidth light control over a 115200 baud UART line. If not, could try faster USB serial or other. No idea. Communication between chandelier control PC and remote control PC doesn't have to be realtime.

2016.12.25
Organizing control code, making more extensible. It's MCU code, so we're more concerned about pure speed and time efficiency than sophisticated organization, but extensibility and maintainability are nice.
Started to write fireflies demo, then remembered that currently we have two dead GPIO pins, which are fixed by connecting the two corresponding light strings to two other GPIO pins. Therefore, we can't do fireflies or anything that is x/y dependent.
Need to read signals on an oscilloscope and figure out why PC4/5 aren't working. No signals coming out of the level shifters.
In order to have single clusters complete and bug free, need:
-fix remaining two channels
-double check all signals in o-scope
-maybe replace existing signal write code with assembly signal write code from Dreamer's head, that's faster and more efficient, first need to check that on scope to tweek timing
