// flies.c
// Firefly visualization for Chandelier.
// This file is part of Chandelier v0.8
// Travis Llado, travis@travisllado.com
// Last modified 2016.12.25

////////////////////////////////////////////////////////////////////////////////
// Dependencies

#include <cstdlib>
#include <stdint.h>
#include "config.h"
#include "flies.h"
#include "tempcoloruint.h"

////////////////////////////////////////////////////////////////////////////////
// updateFlies()
// Produces fireflies visualization. Flies randomly change direction, blinking
//  at random intervals.

void updateFlies(uint32_t *LED, uint8_t brightness) {
    static uint32_t firstRun = 0;

    if(firstRun == 0) {
        for(uint32_t ii = 0; ii < NUM_FLIES; ii++) {
            
        }

        firstRun = 1;
    }
    for(uint32_t ii = 0; ii < NUM_FLIES; ii++) {

    }
}
