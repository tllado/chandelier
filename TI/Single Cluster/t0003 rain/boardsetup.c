// boardsetup.c

#include <stdint.h>
#include <cstdlib>
#include "tm4c123gh6pm.h"
#include "PLL.h"
#include "boardsetup.h"

void DisableInterrupts(void);
void EnableInterrupts(void);
long StartCritical (void);
void EndCritical(long sr);
void WaitForInterrupt(void);

const int framerate = 60;       // Hz
const int bitFreq = 800000;     // Hz
const int clockFreq = 80000000; // Hz
const int bitsPerLight = 24;
const int lightsPerString = 16;
const int numStrings = 16;
const int bitPriority = 1;
const int framePriority = 2;

const int bitPeriod = clockFreq/bitFreq;
const int framePeriod = clockFreq/framerate;
const int signalLength = lightsPerString*bitsPerLight;

int LED[numStrings][lightsPerString] = {0};
int bits[signalLength] = {0};
int volatile delay = 0;
int bitCount = 0;

void PortA_Init(void);
void PortB_Init(void);
int Timer1_Init(void);
int Timer2_Init(void);
void Timer1A_Handler(void);
void Timer2A_Handler(void);
void transformLights(void);
int rainbow(int);
void makeAFewColors(void);

void PortA_Init(void) {
    DisableInterrupts();
        SYSCTL_RCGC2_R |= 0x00000001;   // activate clock for Port A
        delay = SYSCTL_RCGC2_R;         // allow time for clock to start
        GPIO_PORTA_AMSEL_R = 0x00;      // disable analog on PA0-7
        GPIO_PORTA_PCTL_R = 0x00000000; // PCTL GPIO on PA0-7
        GPIO_PORTA_DIR_R = 0xFF;        // PA0-7 out
        GPIO_PORTA_AFSEL_R = 0x00;      // disable alt funct on PA0-7
        GPIO_PORTA_DEN_R = 0xFF;        // enable digital I/O on PA0-7
    EnableInterrupts();
}

void PortB_Init(void) {
    DisableInterrupts();
        SYSCTL_RCGC2_R |= 0x00000002;   // activate clock for Port B
        delay = SYSCTL_RCGC2_R;         // allow time for clock to start
        GPIO_PORTB_AMSEL_R = 0x00;      // disable analog on PB0-7
        GPIO_PORTB_PCTL_R = 0x00000000; // PCTL GPIO on PB0-7
        GPIO_PORTB_DIR_R = 0xFF;        // PB0-7 out
        GPIO_PORTB_AFSEL_R = 0x00;      // disable alt funct on PB0-7
        GPIO_PORTB_DEN_R = 0xFF;        // enable digital I/O on PB0-7
    EnableInterrupts();
}

int Timer1_Init(void) {
    DisableInterrupts();
        SYSCTL_RCGCTIMER_R |= 0x02;         // activate TIMER1
        delay = SYSCTL_RCGCTIMER_R;         // allow time for timer to start
        TIMER1_CTL_R = 0x00000000;          // disable TIMER1A during setup
        TIMER1_CFG_R = 0x00000000;          // set to 32-bit mode
        TIMER1_TAMR_R = 0x00000002;         // set to periodic mode
        TIMER1_TAILR_R = framePeriod - 1;   // set reset value
        TIMER1_TAPR_R = 0;                  // set bus clock resolution
        TIMER1_ICR_R = 0x00000001;          // clear TIMER1A timeout flag
        TIMER1_IMR_R |= 0x00000001;         // arm timeout interrupt
        NVIC_PRI5_R = (NVIC_PRI5_R & 0x00FFFFFF) | (framePriority << 29);
                                            // set priority
        NVIC_EN0_R = 1 << 21;               // enable IRQ 21 in NVIC
        TIMER1_CTL_R = 0x00000001;          // enable TIMER1A
    EnableInterrupts();
  return 1;
}

int Timer2_Init(void) {
    DisableInterrupts();
        SYSCTL_RCGCTIMER_R |= 0x04;     // activate timer2
        delay = SYSCTL_RCGCTIMER_R;     // allow time for timer to start
        TIMER2_CTL_R = 0x00000000;      // disable TIMER2A during setup
        TIMER2_CFG_R = 0x00000000;      // set to 32-bit mode
        TIMER2_TAMR_R = 0x00000002;     // set to periodic mode
        TIMER2_TAILR_R = bitPeriod - 1; // set reset value
        TIMER2_TAPR_R = 0;              // set bus clock resolution
        TIMER2_ICR_R = 0x00000001;      // clear TIMER1A timeout flag
        TIMER2_IMR_R |= 0x00000001;     // arm timeout interrupt
        NVIC_PRI5_R = (NVIC_PRI5_R & 0x00FFFFFF) | (bitPriority << 29);
                                        // set priority
        NVIC_EN0_R = 1 << 23;           // enable interrupt 23 in NVIC
    EnableInterrupts();
    return 1;
}

void Timer1A_Handler(void) {
    TIMER1_ICR_R = 0x00000001;      // acknowledge timer1A timeout
    makeAFewColors();
    transformLights();              // transform LED arrays to bit arrays
    bitCount = signalLength - 1;    // reset message bit counter
    TIMER2_CTL_R = 0x00000001;      // enable Timer 2A
}

void Timer2A_Handler(void) {
    TIMER2_ICR_R = 0x00000001;                  // acknowledge timer2A timeout
    
    GPIO_PORTA_DATA_R = 0x000000FF;             // write all PortA pins high
    delay=0;delay=0;delay=0;
    GPIO_PORTB_DATA_R = 0x000000FF;             // write all PortB pins high
    GPIO_PORTA_DATA_R = bits[bitCount];         // write PortA to message bits
    GPIO_PORTB_DATA_R = bits[bitCount] >> 8;    // write PortB to message bits
    delay=0;delay=0;delay=0;delay=0;delay=0;    // delay remaining 1 time
    GPIO_PORTA_DATA_R = 0x00000000;             // write all PortA pins low
    delay=0;delay=0;delay=0;
    GPIO_PORTB_DATA_R = 0x00000000;             // write all PortB pins low

    bitCount--;
    if(bitCount == 0)                           // after entire message is transmitted
        TIMER2_CTL_R = 0x00000000;              // disable Timer 2A
}

// Transform next frame array of LED values to array of parallel bus bytes
void transformLights(void) {
    int bitIndex = 0;
    for(int lightNum = 0; lightNum < lightsPerString; lightNum++) {
        for(int bitNum = 0; bitNum < bitsPerLight; bitNum++) {
            bitIndex = lightNum*bitsPerLight + bitNum;
            bits[bitIndex] = 0;
            for(int stringNum = 0; stringNum < numStrings; stringNum++) {
                bits[bitIndex] += ((LED[stringNum][lightNum] &
                    (1 << bitNum)) >> bitNum) << stringNum;
            }
        }
    }
}

// Temporary function, write raindrops to top row
const int rainPeriod = 15;
void makeAFewColors(void) {
    for(int lightNum = lightsPerString - 1; lightNum > 0; lightNum--) {
        for(int stringNum = 0; stringNum < numStrings; stringNum++) {
            LED[stringNum][lightNum] = LED[stringNum][lightNum - 1];
        }

        if(rand()%rainPeriod == 1)
            LED[stringNum][0] = onColor;
        else
            LED[stringNum][0] = 0;
    }
}
