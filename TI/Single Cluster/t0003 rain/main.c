#include <stdint.h>
#include "tm4c123gh6pm.h"
#include "PLL.h"
#include "tempcolor.h"
#include "boardsetup.h"

int main(void){
  	PLL_Init();
  	PortA_Init();
  	PortB_Init();
		Timer1_Init();
		Timer2_Init();
  
	while(1){
    // NOTHING HERE!
		// Everything is done using timer interrupt handlers
  }
}
