// GPIO pin setup data

/*

#define PA0	(*((volatile uint32_t *)0x40004004))
#define PA1	(*((volatile uint32_t *)0x40004008))
#define PA2	(*((volatile uint32_t *)0x40004010))
#define PA3	(*((volatile uint32_t *)0x40004020))
#define PA4	(*((volatile uint32_t *)0x40004040))
#define PA5	(*((volatile uint32_t *)0x40004080))
#define PA6	(*((volatile uint32_t *)0x40004100))
#define PA7	(*((volatile uint32_t *)0x40004200))

// A Init
SYSCTL_RCGC2_R |= 0x00000001;     // 1) activate clock for Port A
delay = SYSCTL_RCGC2_R;           // allow time for clock to start
delay = SYSCTL_RCGC2_R;           // allow time for clock to start
GPIO_PORTA_AMSEL_R &= ~0xFF;      // 3) disable analog on PA7
GPIO_PORTA_PCTL_R &= ~0xF0000000; // 4) PCTL GPIO on PA7
GPIO_PORTA_DIR_R |= 0xFF;         // 5) PA7 out
GPIO_PORTA_AFSEL_R &= ~0xFF;      // 6) disable alt funct on PA7
GPIO_PORTA_DEN_R |= 0xFF;         // 7) enable digital I/O on PA7

PortA	0x40004000
PortB	0x40005000
PortC	0x40006000
PortD	0x40007000
PortE	0x40024000
PortF	0x40025000

bit0	0x0004
bit1	0x0008
bit2	0x0010
bit3	0x0020
bit4	0x0040
bit5	0x0080
bit6	0x0100
bit7	0x0200
*/
