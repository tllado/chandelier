// boardsetup.h

// #define PA0 (*((volatile uint32_t *)0x40004004))
// #define PA1 (*((volatile uint32_t *)0x40004008))
// #define PA2 (*((volatile uint32_t *)0x40004010))
// #define PA3 (*((volatile uint32_t *)0x40004020))
// #define PA4 (*((volatile uint32_t *)0x40004040))
// #define PA5 (*((volatile uint32_t *)0x40004080))
// #define PA6 (*((volatile uint32_t *)0x40004100))
// #define PA7 (*((volatile uint32_t *)0x40004200))

// #define PB0 (*((volatile uint32_t *)0x40005004))
// #define PB1 (*((volatile uint32_t *)0x40005008))
// #define PB2 (*((volatile uint32_t *)0x40005010))
// #define PB3 (*((volatile uint32_t *)0x40005020))
// #define PB4 (*((volatile uint32_t *)0x40005040))
// #define PB5 (*((volatile uint32_t *)0x40005080))
// #define PB6 (*((volatile uint32_t *)0x40005100))
// #define PB7 (*((volatile uint32_t *)0x40005200))

void PortA_Init(void);

void PortB_Init(void);

// void PortE_Init(void);

// void PortF_Init(void);

int Timer1_Init(void);

int Timer2_Init(void);

void Timer1A_Handler(void);

void Timer2A_Handler(void);

void waitZero(void);

void waitOne(void);
