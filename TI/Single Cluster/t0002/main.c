#include <stdint.h>
#include "tm4c123gh6pm.h"
#include "PLL.h"
#include "tempcolor.h"
#include "boardsetup.h"

int main(void){
  	PLL_Init();
  	PortA_Init();
  	PortB_Init();
	// PortF_Init();
	Timer1_Init();
	Timer2_Init();
  
	while(1){
    // NOTHING HERE!
		// This code should either calculate light values from simulation
		// or read light values from master controller via UART.
		// To make this calculation/update timing easier, I might put this in
		// an interrupt handler as well. Why not.
  }
}
