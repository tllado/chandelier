// boardsetup.c

#include <stdint.h>
#include "tm4c123gh6pm.h"
#include "PLL.h"
#include "boardsetup.h"

void DisableInterrupts(void);
void EnableInterrupts(void);
long StartCritical (void);
void EndCritical(long sr);
void WaitForInterrupt(void);

const int bitPeriod = 100;          // 80MHz/800KHz = 100 cycles
const int framePeriod = 1333333;    // 80MHz/60fps = 1.333M cycles
const int bitPriority = 3;
const int framePriority = 2;

uint32_t volatile delay = 0;
uint32_t bitCount = 0;
const uint32_t numLights = 16 ;
uint32_t combo = 0x000000AA;
uint32_t oneCycle = 0;

void PortA_Init(void) {
    SYSCTL_RCGC2_R |= 0x00000001;   // activate clock for Port A
    delay = SYSCTL_RCGC2_R;         // allow time for clock to start
    GPIO_PORTA_AMSEL_R = 0x00;      // disable analog on PA0-7
    GPIO_PORTA_PCTL_R = 0x00000000; // PCTL GPIO on PA0-7
    GPIO_PORTA_DIR_R = 0xFF;        // PA0-7 out
    GPIO_PORTA_AFSEL_R = 0x00;      // disable alt funct on PA0-7
    GPIO_PORTA_DEN_R = 0xFF;        // enable digital I/O on PA0-7
}

void PortB_Init(void) {
    SYSCTL_RCGC2_R |= 0x00000002;   // activate clock for Port B
    delay = SYSCTL_RCGC2_R;         // allow time for clock to start
    GPIO_PORTB_AMSEL_R = 0x00;      // disable analog on PB0-7
    GPIO_PORTB_PCTL_R = 0x00000000; // PCTL GPIO on PB0-7
    GPIO_PORTB_DIR_R = 0xFF;        // PB0-7 out
    GPIO_PORTB_AFSEL_R = 0x00;      // disable alt funct on PB0-7
    GPIO_PORTB_DEN_R = 0xFF;        // enable digital I/O on PB0-7
}

int Timer1_Init() {
    DisableInterrupts();
        SYSCTL_RCGCTIMER_R |= 0x02;         // activate TIMER1
        delay = SYSCTL_RCGCTIMER_R;
        TIMER1_CTL_R = 0x00000000;          // disable TIMER1A during setup
        TIMER1_CFG_R = 0x00000000;          // set to 32-bit mode
        TIMER1_TAMR_R = 0x00000002;         // set to periodic mode
        TIMER1_TAILR_R = framePeriod - 1;   // set reset value
        TIMER1_TAPR_R = 0;                  // set bus clock resolution
        TIMER1_ICR_R = 0x00000001;          // clear TIMER1A timeout flag
        TIMER1_IMR_R |= 0x00000001;         // arm timeout interrupt
        NVIC_PRI5_R = (NVIC_PRI5_R & 0x00FFFFFF) | (framePriority << 29); // set priority
        NVIC_EN0_R = 1 << 21;               // enable IRQ 21 in NVIC
        TIMER1_CTL_R = 0x00000001;          // enable TIMER1A
    EnableInterrupts();
  return 1;
}

int Timer2_Init() {
    DisableInterrupts();
        SYSCTL_RCGCTIMER_R |= 0x04;         // activate timer2
        delay = SYSCTL_RCGCTIMER_R;
        TIMER2_CTL_R = 0x00000000;          // disable TIMER2A during setup
        TIMER2_CFG_R = 0x00000000;          // set to 32-bit mode
        TIMER2_TAMR_R = 0x00000002;         // set to periodic mode
        TIMER2_TAILR_R = bitPeriod - 1;     // set reset value
        TIMER2_TAPR_R = 0;                  // set bus clock resolution
        TIMER2_ICR_R = 0x00000001;          // clear TIMER1A timeout flag
        TIMER2_IMR_R |= 0x00000001;         // arm timeout interrupt
        NVIC_PRI5_R = (NVIC_PRI5_R & 0x00FFFFFF) | (bitPriority << 29);   // set priority
        NVIC_EN0_R = 1 << 23;               // enable interrupt 23 in NVIC
    EnableInterrupts();
    return 1;
}

void Timer1A_Handler(void) {
    TIMER1_ICR_R = 0x00000001;  // acknowledge timer1A timeout

    bitCount = numLights * 24;   // reset message bit counter
    TIMER2_CTL_R = 0x00000001;  // enable Timer 2A
}

void Timer2A_Handler(void) {
    TIMER2_ICR_R = 0x00000001;      // acknowledge timer2A timeout

    GPIO_PORTA_DATA_R = 0x000000FF; // write all PortA pins high
    GPIO_PORTB_DATA_R = 0x000000FF; // write all PortB pins high
    oneCycle = 0;                   // wait zero delay time
    GPIO_PORTA_DATA_R = combo;      // write PortA pins to desired positions
    GPIO_PORTB_DATA_R = combo;      // write PortB pins to desired positions
    oneCycle = 0;                   // wait remaining one delay time
    GPIO_PORTA_DATA_R = 0x00000000; // write PortA pins low
    GPIO_PORTB_DATA_R = 0x00000000; // write PortB pins low

    bitCount--;

    if(bitCount == 0)
        TIMER2_CTL_R = 0x00000000;  // disable Timer 2A
}
