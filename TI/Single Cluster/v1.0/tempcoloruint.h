// tempcoloruint.h
// Converts light temperatures from 1,000 K to 40,000 K, as well as desired
// brightness, into 24-bit light color codes for WS2812 LEDs.
// Conversion data is provided by Mitch Charity at Vendian.
// (http://www.vendian.org/mncharity/dir3/blackbody/)

// Chandelier Single Cluster v1.0
// Copyright (c) 2016 Travis Llado, travis@travisllado.com
// Last modified 2016.12.28

// This file is part of Chandelier.
// 
// Chandelier is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
// 
// Chandelier is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with
// Chandelier.  If not, see <http://www.gnu.org/licenses/>.

////////////////////////////////////////////////////////////////////////////////
// tempColor()
// Accepts two values (for temperature and brightness) and returns the
// corresponding neopixel color code (uint32_t). temp=0 is full red (1000K),
// temp=128 is neutral (6600K), and temp=255 is full blue (40000K).

uint32_t tempColor(uint8_t temperature, uint8_t brightness);
