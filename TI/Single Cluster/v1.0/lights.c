 // lightctrl.c
// Initializes and handles hardware and memory required for normal operation of
// ws2812 LEDs for Chandelier.

// Chandelier Single Cluster v1.0
// Copyright (c) 2016 Travis Llado, travis@travisllado.com
// Last modified 2016.12.28

// This file is part of Chandelier.
// 
// Chandelier is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
// 
// Chandelier is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with
// Chandelier.  If not, see <http://www.gnu.org/licenses/>.

////////////////////////////////////////////////////////////////////////////////
// Dependencies

#include <stdint.h>
#include "config.h"
#include "lights.h"

void DisableInterrupts(void);
void EnableInterrupts(void);

////////////////////////////////////////////////////////////////////////////////
// Global Variables

uint32_t bits[SIGNAL_LENGTH] = {0};
uint32_t bitCount = 0;
void (*updateTask)(void);
uint32_t *LEDs;

#define PF1                 (*((volatile uint32_t *)0x40025008))
#define PF2                 (*((volatile uint32_t *)0x40025010))
#define PF3                 (*((volatile uint32_t *)0x40025020))
// GPIO
#define TIMER1_CTL_R            (*((volatile uint32_t *)0x4003100C))
#define SYSCTL_RCGCGPIO_R       (*((volatile uint32_t *)0x400FE608))
#define SYSCTL_RCGCGPIO_R0      0x00000001  // GPIO Port A Run Mode Clock
                                            // Gating Control
#define SYSCTL_PRGPIO_R         (*((volatile uint32_t *)0x400FEA08))
#define SYSCTL_PRGPIO_R0        0x00000001  // GPIO Port A Peripheral Ready
#define GPIO_PORTA_AMSEL_R      (*((volatile uint32_t *)0x40004528))
#define GPIO_PORTA_PCTL_R       (*((volatile uint32_t *)0x4000452C))
#define GPIO_PORTA_DIR_R        (*((volatile uint32_t *)0x40004400))
#define GPIO_PORTA_AFSEL_R      (*((volatile uint32_t *)0x40004420))
#define GPIO_PORTA_DEN_R        (*((volatile uint32_t *)0x4000451C))
#define SYSCTL_RCGCGPIO_R1      0x00000002  // GPIO Port B Run Mode Clock
                                            // Gating Control
#define SYSCTL_PRGPIO_R1        0x00000002  // GPIO Port B Peripheral Ready
#define GPIO_PORTB_AMSEL_R      (*((volatile uint32_t *)0x40005528))
#define GPIO_PORTB_PCTL_R       (*((volatile uint32_t *)0x4000552C))
#define GPIO_PORTB_DIR_R        (*((volatile uint32_t *)0x40005400))
#define GPIO_PORTB_AFSEL_R      (*((volatile uint32_t *)0x40005420))
#define GPIO_PORTB_DEN_R        (*((volatile uint32_t *)0x4000551C))
#define SYSCTL_RCGCGPIO_R2      0x00000004  // GPIO Port C Run Mode Clock
                                            // Gating Control
#define SYSCTL_PRGPIO_R2        0x00000004  // GPIO Port C Peripheral Ready
#define GPIO_PORTC_AMSEL_R      (*((volatile uint32_t *)0x40006528))
#define GPIO_PORTC_PCTL_R       (*((volatile uint32_t *)0x4000652C))
#define GPIO_PORTC_DIR_R        (*((volatile uint32_t *)0x40006400))
#define GPIO_PORTC_AFSEL_R      (*((volatile uint32_t *)0x40006420))
#define GPIO_PORTC_DEN_R        (*((volatile uint32_t *)0x4000651C))
#define GPIO_PORTA_DATA_R       (*((volatile uint32_t *)0x400043FC))
#define GPIO_PORTB_DATA_R       (*((volatile uint32_t *)0x400053FC))
#define GPIO_PORTC_DATA_R       (*((volatile uint32_t *)0x400063FC))
// Timers
#define SYSCTL_RCGCTIMER_R      (*((volatile uint32_t *)0x400FE604))
#define SYSCTL_RCGCTIMER_R1     0x00000002  // Timer 1 Run Mode Clock Gating
                                            // Control
#define SYSCTL_PRTIMER_R        (*((volatile uint32_t *)0x400FEA04))
#define SYSCTL_PRTIMER_R1       0x00000002  // Timer 1 Peripheral Ready
#define TIMER1_CTL_R            (*((volatile uint32_t *)0x4003100C))
#define TIMER1_CFG_R            (*((volatile uint32_t *)0x40031000))
#define TIMER1_TAMR_R           (*((volatile uint32_t *)0x40031004))
#define TIMER1_TAILR_R          (*((volatile uint32_t *)0x40031028))
#define TIMER1_TAPR_R           (*((volatile uint32_t *)0x40031038))
#define TIMER1_ICR_R            (*((volatile uint32_t *)0x40031024))
#define TIMER1_IMR_R            (*((volatile uint32_t *)0x40031018))
#define NVIC_PRI5_R             (*((volatile uint32_t *)0xE000E414))
#define NVIC_EN0_R              (*((volatile uint32_t *)0xE000E100))
#define SYSCTL_RCGCTIMER_R2     0x00000004  // Timer 2 Run Mode Clock Gating
                                            // Control
#define SYSCTL_PRTIMER_R2       0x00000004  // Timer 2 Peripheral Ready
#define TIMER2_CTL_R            (*((volatile uint32_t *)0x4003200C))
#define TIMER2_CFG_R            (*((volatile uint32_t *)0x40032000))
#define TIMER2_TAMR_R           (*((volatile uint32_t *)0x40032004))
#define TIMER2_TAILR_R          (*((volatile uint32_t *)0x40032028))
#define TIMER2_TAPR_R           (*((volatile uint32_t *)0x40032038))
#define TIMER2_ICR_R            (*((volatile uint32_t *)0x40032024))
#define TIMER2_IMR_R            (*((volatile uint32_t *)0x40032018))
#define NVIC_PRI5_R             (*((volatile uint32_t *)0xE000E414))
#define NVIC_EN0_R              (*((volatile uint32_t *)0xE000E100))


////////////////////////////////////////////////////////////////////////////////
// Internal Prototypes

void PortA_Init(void);
void PortB_Init(void);
void PortC_Init(void);
void Timer1_Init(void);
void Timer1A_Handler(void);
void Timer2_Init(void);
void Timer2A_Handler(void);
void transformLights(void);
void updateLights(void);

////////////////////////////////////////////////////////////////////////////////
// clearLEDs()
// Sets all LEDs to be off on next update.

void clearLights(void) {
    for(int ii = 0; ii < NUM_STRINGS*LIGHTS_PER_STRING; ii++)
        LEDs[ii] = 0;
}

////////////////////////////////////////////////////////////////////////////////
// lightsInit()
// Performs all hardware setup for chandelier light string cluster. Takes input
// of function to be performed each frame.

void lightsInit(void (*newFunc)(void), uint32_t *newArray) {
    updateTask = newFunc;
    LEDs = newArray;
     
    PortA_Init();
    PortB_Init();
    PortC_Init();
    Timer1_Init();
    Timer2_Init();

    lightsOn();
    clearLights();
    updateLights();
    lightsOff();
}

////////////////////////////////////////////////////////////////////////////////
// lightsOff()
// Performs all hardware setup for chandelier light string cluster. Takes input
// of function to be performed each frame.

void lightsOff(void) {
    TIMER1_CTL_R = 0x00;    // disable TIMER1A
}

////////////////////////////////////////////////////////////////////////////////
// lightsOn()
// Performs all hardware setup for chandelier light string cluster. Takes input
// of function to be performed each frame.

void lightsOn(void) {
    TIMER1_CTL_R = 0x01;    // enable TIMER1A
}

////////////////////////////////////////////////////////////////////////////////
// PortA_Init()
// Initializes all hardware needed to use Port A GPIO pins 2-7

void PortA_Init(void) {
    DisableInterrupts();
        SYSCTL_RCGCGPIO_R |= SYSCTL_RCGCGPIO_R0;
                                            // activate clock for Port A
        while((SYSCTL_PRGPIO_R & SYSCTL_PRGPIO_R0) == 0) {}
                                            // wait for clock to start
        GPIO_PORTA_AMSEL_R &= ~0xFC;        // disable analog on PA0-7
        GPIO_PORTA_PCTL_R &= ~0xFFFFFF00;	// PCTL GPIO on PA0-7
        GPIO_PORTA_DIR_R |= 0xFC;           // PA0-7 out
        GPIO_PORTA_AFSEL_R &= ~0xFC;        // disable alt funct on PA0-7
        GPIO_PORTA_DEN_R |= 0xFC;           // enable digital I/O on PA0-7
    EnableInterrupts();
}

////////////////////////////////////////////////////////////////////////////////
// PortB_Init()
// Initializes all hardware needed to use Port B GPIO pins

void PortB_Init(void) {
    DisableInterrupts();
        SYSCTL_RCGCGPIO_R |= SYSCTL_RCGCGPIO_R1;
                                            // activate clock for Port B
        while((SYSCTL_PRGPIO_R & SYSCTL_PRGPIO_R1) == 0) {}
                                            // wait for clock to start
        GPIO_PORTB_AMSEL_R &= ~0xFF;     	// disable analog on PB0-7
        GPIO_PORTB_PCTL_R &= ~0xFFFFFFFF;	// PCTL GPIO on PB0-7
        GPIO_PORTB_DIR_R |= 0xFF;           // PB0-7 out
        GPIO_PORTB_AFSEL_R &= ~0xFF;        // disable alt funct on PB0-7
        GPIO_PORTB_DEN_R |= 0xFF;           // enable digital I/O on PB0-7
    EnableInterrupts();
}

////////////////////////////////////////////////////////////////////////////////
// PortC_Init()
// Initializes all hardware needed to use Port C GPIO pins 4-7

void PortC_Init(void) {
    DisableInterrupts();
        SYSCTL_RCGCGPIO_R |= SYSCTL_RCGCGPIO_R2;
                                            // activate port C
        while((SYSCTL_PRGPIO_R & SYSCTL_PRGPIO_R2) == 0) {}
                                            // wait for clock to stabilize
        GPIO_PORTC_AMSEL_R &= ~0xF0;        // disable analog funcs on PC4-7
        GPIO_PORTC_PCTL_R &= ~0xFFFF0000;   // configure PC4-7 as GPIO
        GPIO_PORTC_DIR_R |= 0xF0;           // make PC4-7 output
        GPIO_PORTC_AFSEL_R &= ~0xF0;        // disable alt funct on PC4-7
        GPIO_PORTC_DEN_R |= 0xF0;           // enable digital I/O on PC4-7
    EnableInterrupts();
}

////////////////////////////////////////////////////////////////////////////////
// Timer1_Init()
// Initializes all hardware needed to use Timer 1A for frame update

void Timer1_Init(void) {
    DisableInterrupts();
        SYSCTL_RCGCTIMER_R |= SYSCTL_RCGCTIMER_R1;
                                            // activate TIMER1
        while((SYSCTL_PRTIMER_R & SYSCTL_PRTIMER_R1) == 0) {}
                                            // wait for timer to start
        TIMER1_CTL_R = 0x00;                // disable TIMER1A during setup
        TIMER1_CFG_R = 0x00;                // set to 32-bit mode
        TIMER1_TAMR_R = 0x02;               // set to periodic mode
        TIMER1_TAILR_R = FRAME_PERIOD - 1;  // set reset value
        TIMER1_TAPR_R = 0;                  // set bus clock resolution
        TIMER1_ICR_R = 0x01;                // clear TIMER1A timeout flag
        TIMER1_IMR_R |= 0x01;               // arm timeout interrupt
        NVIC_PRI5_R = (NVIC_PRI5_R & 0x00FFFFFF) | (FRAME_PRIORITY << 29);
                                            // set priority
        NVIC_EN0_R = 1 << 21;               // enable IRQ 21 in NVIC
    EnableInterrupts();
}

////////////////////////////////////////////////////////////////////////////////
// Starts sequence of visualization and LED update for a single frame.

void Timer1A_Handler(void) {
    TIMER1_ICR_R = 0x01;    // acknowledge timer1A timeout

    updateTask();
    updateLights();
}

////////////////////////////////////////////////////////////////////////////////
// Timer2_Init()
// Initializes all hardware needed to use Timer2A for bit updates

void Timer2_Init(void) {
    DisableInterrupts();
        SYSCTL_RCGCTIMER_R |= SYSCTL_RCGCTIMER_R2;
                                            // activate timer2
        while((SYSCTL_PRTIMER_R & SYSCTL_PRTIMER_R2) == 0) {}
                                            // wait for timer to start
        TIMER2_CTL_R = 0x00;                // disable TIMER2A during setup
        TIMER2_CFG_R = 0x00;                // set to 32-bit mode
        TIMER2_TAMR_R = 0x02;               // set to periodic mode
        TIMER2_TAILR_R = BIT_PERIOD - 1;    // set reset value
        TIMER2_TAPR_R = 0;                  // set bus clock resolution
        TIMER2_ICR_R = 0x01;                // clear TIMER1A timeout flag
        TIMER2_IMR_R |= 0x01;               // arm timeout interrupt
        NVIC_PRI5_R = (NVIC_PRI5_R & 0x00FFFFFF) | (BIT_PRIORITY << 29);
                                            // set priority
        NVIC_EN0_R = 1 << 23;               // enable interrupt 23 in NVIC
    EnableInterrupts();
}

////////////////////////////////////////////////////////////////////////////////
// Timer2A_Handler()
// Writes single bit to all LED strings

void Timer2A_Handler(void) {
    uint32_t volatile spin = 0;
    uint32_t thisMessage = bits[bitCount];
    uint32_t ABits = (thisMessage&0x00FC)|(~0x00FC);
    uint32_t BBits = (thisMessage&0xFF00)>>8;
    uint32_t CBits = ((thisMessage&0x0003)<<4)|(~0x0030);

    TIMER2_ICR_R = 0x01;        // acknowledge timer2A timeout
    
    GPIO_PORTA_DATA_R |= 0xFC;  // write all PortA pins high
    GPIO_PORTB_DATA_R |= 0xFF;  // write all PortB pins high
    GPIO_PORTC_DATA_R |= 0x30;  // write all PortB pins high

    spin=0;spin=0;              // wait remaining 0 time
    
    GPIO_PORTA_DATA_R &= ABits; // write PortA to message bits
    GPIO_PORTB_DATA_R &= BBits; // write PortB to message bits
    GPIO_PORTC_DATA_R &= CBits; // write PortB to message bits
    
    spin=0;spin=0;spin=0;spin=0;spin=0;spin=0;spin=0;spin=0;spin=0;spin=0;
    spin=0;spin=0;spin=0;spin=0;spin=0;spin=0;spin=0;spin=0;spin=0;spin=0;
    spin=0;spin=0;              // wait remaining 1 time
    
    GPIO_PORTA_DATA_R &= ~0xFC; // write all PortA pins low
    GPIO_PORTB_DATA_R &= ~0xFF; // write all PortB pins low
    GPIO_PORTC_DATA_R &= ~0x30; // write all portC pins low

    bitCount--;
    if(bitCount == 0)           // after entire message is sent
        TIMER2_CTL_R = 0x00;    // disable Timer 2A
}

////////////////////////////////////////////////////////////////////////////////
// transformLights()
// Transforms array of 256 LED color values into array of (16*24) parallel GPIO
// values

void transformLights(void) {
    int bitIndex = 0;
    for(int lightNum = 0; lightNum < LIGHTS_PER_STRING; lightNum++) {
        for(int bitNum = 0; bitNum < BITS_PER_LIGHT; bitNum++) {
            bitIndex = lightNum*BITS_PER_LIGHT + bitNum;
            bits[bitIndex] = 0;
            for(int stringNum = 0; stringNum < NUM_STRINGS; stringNum++) {
                bits[bitIndex] += ((LEDs[stringNum*LIGHTS_PER_STRING           \
                    + lightNum] & (1 << bitNum)) >> bitNum) << stringNum;
            }
        }
    }
}

////////////////////////////////////////////////////////////////////////////////
// updateLights()
// Performs actions to commence writing of current contents of LED[] to LEDs.

void updateLights(void) {
    transformLights();
    bitCount = SIGNAL_LENGTH - 1;   // reset message bit counter
    TIMER2_CTL_R = 0x01;            // enable Timer 2A
}
