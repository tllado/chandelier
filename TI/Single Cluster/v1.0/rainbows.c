// rainbows.c
// Rainbows visualization for Chandelier

// Chandelier Single Cluster v1.0
// Copyright (c) 2016 Travis Llado, travis@travisllado.com
// Last modified 2016.12.28

// This file is part of Chandelier.
// 
// Chandelier is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
// 
// Chandelier is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with
// Chandelier.  If not, see <http://www.gnu.org/licenses/>.

////////////////////////////////////////////////////////////////////////////////
// Dependencies

#include <stdint.h>
#include "config.h"
#include "rainbows.h"

////////////////////////////////////////////////////////////////////////////////
// Internal Prototypes

uint32_t rainbowHue(uint8_t hue, uint8_t brightness);

////////////////////////////////////////////////////////////////////////////////
// rainbowHue()
// Create 24-bit color code from 8-bit hue

uint32_t rainbowHue(uint8_t hue, uint8_t brightness) {
    hue = 255 - hue;
    if(hue < 85)
        return ((255 - hue*3)*brightness/256 << 16) + (hue*3)*brightness/256;
    if(hue < 170) {
        hue -= 85;
        return ((hue*3)*brightness/256 << 8) + (255 - hue*3)*brightness/256;
    }
    hue -= 170;
    return ((hue*3)*brightness/256 << 16) + ((255 - hue*3)*brightness/256 << 8);
}

////////////////////////////////////////////////////////////////////////////////
// updateRainbow()
// Produces rainbow visualization. Rainbow cycles flow steadily downward.

void updateRainbow(uint32_t *LED, uint8_t brightness) {
    static uint8_t rainbowColor = 0;
    static uint8_t nextColor = 0;
    int thisColor[LIGHTS_PER_STRING] = {rainbowHue(rainbowColor, brightness)};
    
    for(int ii = 1; ii < LIGHTS_PER_STRING; ii++) {
        nextColor = rainbowColor + ii*LIGHTS_PER_STRING;
        thisColor[ii] = rainbowHue(nextColor, brightness);
    }
    for(int ii = 0; ii < LIGHTS_PER_STRING; ii++) {
        for(int jj = 0; jj < LIGHTS_PER_STRING; jj++)
            LED[ii*LIGHTS_PER_STRING + LIGHTS_PER_STRING - jj] = thisColor[(LIGHTS_PER_STRING + ii - jj)%LIGHTS_PER_STRING];
    }
    rainbowColor++;
}
