// rainbows.h
// Rainbows visualization for Chandelier

// Chandelier Single Cluster v1.0
// Copyright (c) 2016 Travis Llado, travis@travisllado.com
// Last modified 2016.12.28

// This file is part of Chandelier.
// 
// Chandelier is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
// 
// Chandelier is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with
// Chandelier.  If not, see <http://www.gnu.org/licenses/>.

////////////////////////////////////////////////////////////////////////////////
// Exported Functions

void updateRainbow(uint32_t *newArray, uint8_t brite);
