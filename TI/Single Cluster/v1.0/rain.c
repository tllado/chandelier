// rain.c
// Rain visualization for Chandelier

// Chandelier Single Cluster v1.0
// Copyright (c) 2016 Travis Llado, travis@travisllado.com
// Last modified 2016.12.28

// This file is part of Chandelier.
// 
// Chandelier is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
// 
// Chandelier is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with
// Chandelier.  If not, see <http://www.gnu.org/licenses/>.

////////////////////////////////////////////////////////////////////////////////
// Dependencies

#include <stdint.h>
#include <stdlib.h>
#include "config.h"
#include "rain.h"
#include "tempcoloruint.h"

////////////////////////////////////////////////////////////////////////////////
// updateRain()
// Produces rain visualization. Raindrops fall vertically at user defined
// frequency, density, and speed.

void updateRain(uint32_t *LED, uint8_t brightness) {
    static uint32_t rainCounter = 0;
    
    if(rainCounter == RAIN_PERIOD) {
        rainCounter = 0;
        
        uint32_t onColor;
        onColor = tempColor(RAIN_COLOR, brightness);

        for(int stringNum = 0; stringNum < NUM_STRINGS; stringNum++) {
            if(rand()%RAIN_PROB == 1)
                LED[stringNum*LIGHTS_PER_STRING + LIGHTS_PER_STRING - 1] = onColor;
            else
                LED[stringNum*LIGHTS_PER_STRING + LIGHTS_PER_STRING - 1] = 0;
                
            for(int lightNum = 0; lightNum < LIGHTS_PER_STRING - 1; lightNum++)
                LED[stringNum*LIGHTS_PER_STRING + lightNum] = LED[stringNum*LIGHTS_PER_STRING + lightNum + 1];
        }
    }
    rainCounter++;
}
