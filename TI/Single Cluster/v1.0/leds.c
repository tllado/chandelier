// leds.c
// Initializes and handles LEDs onboard TI TM4C MCU.

// Chandelier Single Cluster v1.0
// Copyright (c) 2016 Travis Llado, travis@travisllado.com
// Last modified 2016.12.28

// This file is part of Chandelier.
// 
// Chandelier is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
// 
// Chandelier is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with
// Chandelier.  If not, see <http://www.gnu.org/licenses/>.

////////////////////////////////////////////////////////////////////////////////
// Dependencies

#include <stdint.h>
#include "leds.h"

void DisableInterrupts(void);
void EnableInterrupts(void);

////////////////////////////////////////////////////////////////////////////////
// Global Variables

#define PF1                 (*((volatile uint32_t *)0x40025008))
#define PF2                 (*((volatile uint32_t *)0x40025010))
#define PF3                 (*((volatile uint32_t *)0x40025020))

#define SYSCTL_RCGCGPIO_R       (*((volatile uint32_t *)0x400FE608))
#define SYSCTL_RCGCGPIO_R5      0x00000020  // GPIO Port F Run Mode Clock
                                            // Gating Control
#define SYSCTL_PRGPIO_R         (*((volatile uint32_t *)0x400FEA08))
#define SYSCTL_PRGPIO_R5        0x00000020  // GPIO Port F Peripheral Ready
#define GPIO_PORTF_DIR_R        (*((volatile uint32_t *)0x40025400))
#define GPIO_PORTF_AFSEL_R      (*((volatile uint32_t *)0x40025420))
#define GPIO_PORTF_DEN_R        (*((volatile uint32_t *)0x4002551C))
#define GPIO_PORTF_PCTL_R       (*((volatile uint32_t *)0x4002552C))
#define GPIO_PORTF_AMSEL_R      (*((volatile uint32_t *)0x40025528))


////////////////////////////////////////////////////////////////////////////////

void LEDsInit(void) {
    DisableInterrupts();
        SYSCTL_RCGCGPIO_R |= SYSCTL_RCGCGPIO_R5;
                                            // activate port F
        while((SYSCTL_PRGPIO_R & SYSCTL_PRGPIO_R5) == 0) {}
                                            // wait for clock to stabilize
        GPIO_PORTF_DIR_R |= 0x0E;           // make PF1-3 output
        GPIO_PORTF_AFSEL_R &= ~0x0E;        // disable alt funct on PF1-3
        GPIO_PORTF_DEN_R |= 0x0E;           // enable digital I/O on PF1-3
        GPIO_PORTF_PCTL_R &= ~0x0000FFF0;   // configure PF1-3 as GPIO
        GPIO_PORTF_AMSEL_R &= ~0x0E;        // disable analog funcs on PF1-3
    EnableInterrupts();
}

////////////////////////////////////////////////////////////////////////////////

void redLEDOn(void) {
    PF1 |= 0x02;
}

////////////////////////////////////////////////////////////////////////////////

void redLEDOff(void) {
    PF1 &= ~0x02;
}

////////////////////////////////////////////////////////////////////////////////

void redLEDToggle(void) {
    PF1 ^= 0x02;
}

////////////////////////////////////////////////////////////////////////////////

void greenLEDOn(void) {
    PF3 |= 0x08;
}

////////////////////////////////////////////////////////////////////////////////

void greenLEDOff(void) {
    PF3 &= ~0x08;
}

////////////////////////////////////////////////////////////////////////////////

void greenLEDToggle(void) {
    PF3 ^= 0x08;
}

////////////////////////////////////////////////////////////////////////////////

void blueLEDOn(void) {
    PF2 |= 0x04;
}

////////////////////////////////////////////////////////////////////////////////

void blueLEDOff(void) {
    PF2 &= ~0x04;
}

////////////////////////////////////////////////////////////////////////////////

void blueLEDToggle(void) {
    PF2 ^= 0x04;
}
