// Stars.h
// Stars visualization for Chandelier

// Chandelier Single Cluster v1.0
// Copyright (c) 2016 Travis Llado, travis@travisllado.com
// Last modified 2016.12.28

// This file is part of Chandelier.
// 
// Chandelier is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
// 
// Chandelier is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with
// Chandelier.  If not, see <http://www.gnu.org/licenses/>.

////////////////////////////////////////////////////////////////////////////////
// User Settings

#define STAR_PERIOD 1       // 1/update_freq
#define NUM_STARS   50      // # visible per cluster
#define MIN_COLOR   70      // 0-255, color temperature
#define MAX_COLOR   165     // 0-255, color temperature
#define MIN_LENGTH  400     // 1/256 seconds
#define MAX_LENGTH  4000    // 1/256 seconds
#define MIN_BRIGHT  50      // 0-255
#define MAX_BRIGHT  255     // 0-255

////////////////////////////////////////////////////////////////////////////////
// Exported Functions

void updateStars(uint32_t *LED, uint8_t brightness);
