// stars.c
// Stars visualization for Chandelier

// Chandelier Single Cluster v1.0
// Copyright (c) 2016 Travis Llado, travis@travisllado.com
// Last modified 2016.12.28

// This file is part of Chandelier.
// 
// Chandelier is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
// 
// Chandelier is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with
// Chandelier.  If not, see <http://www.gnu.org/licenses/>.

////////////////////////////////////////////////////////////////////////////////
// Dependencies

#include <stdlib.h>
#include <stdint.h>
#include "config.h"
#include "fastTrig.h"
#include "stars.h"
#include "tempcoloruint.h"

////////////////////////////////////////////////////////////////////////////////
// updateStars()
// Produces starfield visualization. Stars twinkle on and off with random
//  position, brightness, and period.

void updateStars(uint32_t *LED, uint8_t brightness) {
    static uint32_t updateCounter = 0;
    static uint32_t starBrightness[NUM_STARS] = {0};
    static uint32_t starColor[NUM_STARS] = {0};
    static uint32_t starCount[NUM_STARS] = {0};
    static uint32_t starLED[NUM_STARS] = {0};
    static uint32_t starLength[NUM_STARS] = {0};
    
    // if it's time to update frame
    if(updateCounter == 0) {
        updateCounter = STAR_PERIOD;
        
        for(uint32_t ii = 0; ii < NUM_STARS; ii++) {
            // if counter reaches zero, reinitialize single star
            if(starCount[ii] == 0) {
                // set brightness of previous light to zero
                LED[starLED[ii]] = 0;
                // select new star parameters
                // Don't assign two stars to one LED
                do {
                    starLED[ii] = rand()%(NUM_STRINGS*LIGHTS_PER_STRING);
                } while(LED[starLED[ii]] != 0);
                starLength[ii] = (rand()%(MAX_LENGTH - MIN_LENGTH)             \
                    + MIN_LENGTH)*FRAMERATE/256;
                starBrightness[ii] = rand()%(MAX_BRIGHT - MIN_BRIGHT)          \
                    + MIN_BRIGHT;
                starColor[ii] = rand()%256;
                starCount[ii] = starLength[ii];
            }

            // update star brightness
            uint32_t thisBrightness = brightness*starBrightness[ii]            \
                *cos8(256*starCount[ii]/starLength[ii] + 128)/255/255;

            // write new brightness value to LED array
            LED[starLED[ii]] = tempColor(MIN_COLOR + (MAX_COLOR - MIN_COLOR)   \
                *starColor[ii]/255, thisBrightness);
            
            starCount[ii]--;
        }
    }
    updateCounter--;
}
