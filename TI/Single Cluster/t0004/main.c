// main.c
// Travis Llado
// 2016.05.16

////////////////////////////////////////////////////////////////////////////////
// Dependencies

#include <cstdlib>
#include <stdint.h>
#include "boardsetup.h"
#include "config.h"
#include "PLL.h"
#include "Switches.h"
#include "tempcolor.h"
#include "tm4c123gh6pm.h"
#include "LEDs.h"

////////////////////////////////////////////////////////////////////////////////
// Global Variables

uint8_t  programNum = 0;
uint8_t  brightness = START_BRIGHTNESS;
uint32_t LED[NUM_STRINGS][LIGHTS_PER_STRING] = {0};
uint32_t bits[SIGNAL_LENGTH] = {0};
uint32_t bitCount = 0;

////////////////////////////////////////////////////////////////////////////////
// Internal Prototypes

void GPIOPortF_Handler(void);
uint32_t rainbowHue(int hue);
void Timer1A_Handler(void);
void Timer2A_Handler(void);
void transformLights(void);
void updateRain(void);
void updateRainbow(void);

////////////////////////////////////////////////////////////////////////////////
// GPIOPortF_Handler()
// Decides what action to take for any interrupt on Port F. This includes the
//  Launchpad's two onboard switches.
// Input: none
// Output: none

void GPIOPortF_Handler(void) {
    if(GPIO_PORTF_RIS_R & 0x10) {   // poll PF4(SW1)
        GPIO_PORTF_ICR_R = 0x10;    // acknowledge flag4
        brightness = (brightness + BRIGHTNESS_INCREMENT)%MAX_BRIGHTNESS;
    }
    if(GPIO_PORTF_RIS_R & 0x01) {   // poll PF0(SW2)
        GPIO_PORTF_ICR_R = 0x01;    // acknowledge flag0
        programNum ^= 1;
        redLEDToggle();
    }
}

////////////////////////////////////////////////////////////////////////////////

void Timer1A_Handler(void) {
    static uint32_t rainCounter = 0;

    TIMER1_ICR_R = 0x01;                    // acknowledge timer1A timeout
    
    if(programNum == 0) {
        rainCounter++;
        if(rainCounter == RAIN_SPEED) {
            rainCounter = 0;
            updateRain();
            bitCount = SIGNAL_LENGTH - 1;   // reset message bit counter
            TIMER2_CTL_R = 0x01;            // enable Timer 2A
        }
    }
    else {
        updateRainbow();
        bitCount = SIGNAL_LENGTH - 1;       // reset message bit counter
        TIMER2_CTL_R = 0x01;                // enable Timer 2A
    }
}

////////////////////////////////////////////////////////////////////////////////

void Timer2A_Handler(void) {
    uint32_t volatile delay = 0;

    TIMER2_ICR_R = 0x01;                        // acknowledge timer2A timeout
    
    GPIO_PORTA_DATA_R = 0xFF;                   // write all PortA pins high
    delay=0;delay=0;delay=0;
    GPIO_PORTB_DATA_R = 0xFF;                   // write all PortB pins high
    GPIO_PORTA_DATA_R = bits[bitCount];         // write PortA to message bits
    GPIO_PORTB_DATA_R = bits[bitCount] >> 8;    // write PortB to message bits
    delay=0;delay=0;delay=0;delay=0;delay=0;    // delay remaining 1 time
    GPIO_PORTA_DATA_R = 0x00;                   // write all PortA pins low
    delay=0;delay=0;delay=0;
    GPIO_PORTB_DATA_R = 0x00;                   // write all PortB pins low

    bitCount--;
    if(bitCount == 0)                           // after entire message is sent
        TIMER2_CTL_R = 0x00;                    // disable Timer 2A
}

////////////////////////////////////////////////////////////////////////////////
// Temporary function, draw some raindrops

void updateRain(void) {
    static uint8_t onColor = 0;

    onColor = tempColor(RAIN_COLOR, 1.0*brightness/255);

    for(int stringNum = 0; stringNum < NUM_STRINGS; stringNum++) {
        if(rand()%RAIN_PERIOD == 1)
            LED[stringNum][LIGHTS_PER_STRING - 1] = onColor;
        else
            LED[stringNum][LIGHTS_PER_STRING - 1] = 0;
                
        for(int lightNum = 0; lightNum < LIGHTS_PER_STRING - 1; lightNum++)
            LED[stringNum][lightNum] = LED[stringNum][lightNum + 1];
    }        
    transformLights();
}

////////////////////////////////////////////////////////////////////////////////
// rainbowHue()
// Create 24-bit color code from 8-bit hue

uint32_t rainbowHue(int hue) {
    hue = 255 - hue;
    if(hue < 85)
        return ((255 - hue*3)*brightness/256 << 16) + (hue*3)*brightness/256;
    if(hue < 170) {
        hue -= 85;
        return ((hue*3)*brightness/256 << 8) + (255 - hue*3)*brightness/256;
    }
    hue -= 170;
    return ((hue*3)*brightness/256 << 16) + ((255 - hue*3)*brightness/256 << 8);
}

////////////////////////////////////////////////////////////////////////////////
// Temporary function, write rainbow colors to all ports

void updateRainbow(void) {
    const int magicNum = 16;
    static uint8_t rainbowColor = 0;
    static uint8_t nextColor = 0;
    int thisColor[magicNum] = {rainbowHue(rainbowColor)};
    
        for(int a = 1; a < magicNum; a++) {
        nextColor = rainbowColor + a*magicNum;
        thisColor[a] = rainbowHue(nextColor);
    }
    for(int i = 0; i < magicNum; i++) {
        for(int j = 0; j < magicNum; j++)
            LED[i][magicNum - j] = thisColor[(magicNum + i - j)%magicNum];
    }
    rainbowColor++;
        
        transformLights();
}

////////////////////////////////////////////////////////////////////////////////
// Transform next frame array of LED values to array of parallel bus bytes

void transformLights(void) {
    int bitIndex = 0;
    for(int lightNum = 0; lightNum < LIGHTS_PER_STRING; lightNum++) {
        for(int bitNum = 0; bitNum < BITS_PER_LIGHT; bitNum++) {
            bitIndex = lightNum*BITS_PER_LIGHT + bitNum;
            bits[bitIndex] = 0;
            for(int stringNum = 0; stringNum < NUM_STRINGS; stringNum++) {
                bits[bitIndex] += ((LED[stringNum][lightNum] &
                    (1 << bitNum)) >> bitNum) << stringNum;
            }
        }
    }
}

////////////////////////////////////////////////////////////////////////////////

int main(void) {
    PLL_Init();
    PortA_Init();
    PortB_Init();
    Switches_Init();
    Timer1_Init();
    Timer2_Init();
    LEDs_Init();
    
    while(1) {
        // NOTHING HERE!
        // Everything is done using timer interrupt handlers
    }
}
