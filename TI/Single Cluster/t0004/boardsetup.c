// boardsetup.c
// Travis Llado
// 2016.05.16

////////////////////////////////////////////////////////////////////////////////
// Dependencies

#include <stdint.h>

#include "boardsetup.h"
#include "tm4c123gh6pm.h"

void DisableInterrupts(void);
void EnableInterrupts(void);
long StartCritical (void);
void EndCritical(long sr);
void WaitForInterrupt(void);

////////////////////////////////////////////////////////////////////////////////
// Internal Prototypes

// none

////////////////////////////////////////////////////////////////////////////////

void PortA_Init(void) {
    DisableInterrupts();
        SYSCTL_RCGC2_R |= 0x01;         // activate clock for Port A
        while((SYSCTL_PRGPIO_R&0x01) == 0) {}
                                        // allow time for clock to start
        GPIO_PORTA_AMSEL_R = 0x00;      // disable analog on PA0-7
        GPIO_PORTA_PCTL_R = 0x00;       // PCTL GPIO on PA0-7
        GPIO_PORTA_DIR_R = 0xFF;        // PA0-7 out
        GPIO_PORTA_AFSEL_R = 0x00;      // disable alt funct on PA0-7
        GPIO_PORTA_DEN_R = 0xFF;        // enable digital I/O on PA0-7
    EnableInterrupts();
}

////////////////////////////////////////////////////////////////////////////////

void PortB_Init(void) {
    DisableInterrupts();
        SYSCTL_RCGC2_R |= 0x02;         // activate clock for Port B
        while((SYSCTL_PRGPIO_R&0x02) == 0) {}
                                        // wait for clock to start
        GPIO_PORTB_AMSEL_R = 0x00;      // disable analog on PB0-7
        GPIO_PORTB_PCTL_R = 0x00;       // PCTL GPIO on PB0-7
        GPIO_PORTB_DIR_R = 0xFF;        // PB0-7 out
        GPIO_PORTB_AFSEL_R = 0x00;      // disable alt funct on PB0-7
        GPIO_PORTB_DEN_R = 0xFF;        // enable digital I/O on PB0-7
    EnableInterrupts();
}

////////////////////////////////////////////////////////////////////////////////

void Timer1_Init(void) {
    DisableInterrupts();
        SYSCTL_RCGCTIMER_R |= 0x02;         // activate TIMER1
        while(SYSCTL_RCGCTIMER_R == 0) {}   // wait for timer to start
        TIMER1_CTL_R = 0x00;                // disable TIMER1A during setup
        TIMER1_CFG_R = 0x00;                // set to 32-bit mode
        TIMER1_TAMR_R = 0x02;               // set to periodic mode
        TIMER1_TAILR_R = FRAME_PERIOD - 1;  // set reset value
        TIMER1_TAPR_R = 0;                  // set bus clock resolution
        TIMER1_ICR_R = 0x01;                // clear TIMER1A timeout flag
        TIMER1_IMR_R |= 0x01;               // arm timeout interrupt
        NVIC_PRI5_R = (NVIC_PRI5_R & 0x00FFFFFF) | (FRAME_PRIORITY << 29);
                                            // set priority
        NVIC_EN0_R = 1 << 21;               // enable IRQ 21 in NVIC
        TIMER1_CTL_R = 0x01;                // enable TIMER1A
    EnableInterrupts();
}

////////////////////////////////////////////////////////////////////////////////

void Timer2_Init(void) {
    DisableInterrupts();
        SYSCTL_RCGCTIMER_R |= 0x04;         // activate timer2
        while(SYSCTL_RCGCTIMER_R == 0) {}   // wait for timer to start
        TIMER2_CTL_R = 0x00;                // disable TIMER2A during setup
        TIMER2_CFG_R = 0x00;                // set to 32-bit mode
        TIMER2_TAMR_R = 0x02;               // set to periodic mode
        TIMER2_TAILR_R = BIT_PERIOD - 1;    // set reset value
        TIMER2_TAPR_R = 0;                  // set bus clock resolution
        TIMER2_ICR_R = 0x01;                // clear TIMER1A timeout flag
        TIMER2_IMR_R |= 0x01;               // arm timeout interrupt
        NVIC_PRI5_R = (NVIC_PRI5_R & 0x00FFFFFF) | (BIT_PRIORITY << 29);
                                            // set priority
        NVIC_EN0_R = 1 << 23;               // enable interrupt 23 in NVIC
    EnableInterrupts();
}
