// config.h
// Travis Llado
// 2016.05.16

////////////////////////////////////////////////////////////////////////////////
// User Settings

#define MAX_BRIGHTNESS       90
#define START_BRIGHTNESS     25
#define BRIGHTNESS_INCREMENT 10
#define RAIN_COLOR           -0.1
#define RAIN_PERIOD          15
#define RAIN_SPEED           2
