// rain.c
// Travis Llado
// 2016.05.26

////////////////////////////////////////////////////////////////////////////////
// Dependencies

#include <cstdlib>
#include <stdint.h>

#include "config.h"
#include "rain.h"
#include "tempcoloruint.h"

////////////////////////////////////////////////////////////////////////////////
// updateRain()
// Produces rain visualization. Raindrops fall vertically at user defined
// frequency, density, and speed.

void updateRain(uint32_t *LED, uint8_t brightness) {
    static uint32_t rainCounter = 0;
    
    if(rainCounter == RAIN_PERIOD) {
        rainCounter = 0;
        
        uint32_t onColor;
        onColor = tempColor(RAIN_COLOR, brightness);

        for(int stringNum = 0; stringNum < NUM_STRINGS; stringNum++) {
            if(rand()%RAIN_PROB == 1)
                LED[stringNum*LIGHTS_PER_STRING + LIGHTS_PER_STRING - 1] = onColor;
            else
                LED[stringNum*LIGHTS_PER_STRING + LIGHTS_PER_STRING - 1] = 0;
                
            for(int lightNum = 0; lightNum < LIGHTS_PER_STRING - 1; lightNum++)
                LED[stringNum*LIGHTS_PER_STRING + lightNum] = LED[stringNum*LIGHTS_PER_STRING + lightNum + 1];
        }
    }
    rainCounter++;
}
