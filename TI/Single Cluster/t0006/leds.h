// leds.h
// Travis Llado
// 2016.05.25

////////////////////////////////////////////////////////////////////////////////
// External Prototypes

void LEDsInit(void);
void redLEDOn(void);
void redLEDOff(void);
void redLEDToggle(void);
void greenLEDOn(void);
void greenLEDOff(void);
void greenLEDToggle(void);
void blueLEDOn(void);
void blueLEDOff(void);
void blueLEDToggle(void);
