// rainbows.c
// Travis Llado
// 2016.05.26

////////////////////////////////////////////////////////////////////////////////
// Dependencies

#include <stdint.h>

#include "config.h"
#include "rainbows.h"

////////////////////////////////////////////////////////////////////////////////
// Internal Prototypes

uint32_t rainbowHue(uint8_t hue, uint8_t brightness);

////////////////////////////////////////////////////////////////////////////////
// rainbowHue()
// Create 24-bit color code from 8-bit hue

uint32_t rainbowHue(uint8_t hue, uint8_t brightness) {
    hue = 255 - hue;
    if(hue < 85)
        return ((255 - hue*3)*brightness/256 << 16) + (hue*3)*brightness/256;
    if(hue < 170) {
        hue -= 85;
        return ((hue*3)*brightness/256 << 8) + (255 - hue*3)*brightness/256;
    }
    hue -= 170;
    return ((hue*3)*brightness/256 << 16) + ((255 - hue*3)*brightness/256 << 8);
}

////////////////////////////////////////////////////////////////////////////////
// updateRainbow()
// Produces rainbow visualization. Rainbow cycles flow steadily downward.

void updateRainbow(uint32_t *LED, uint8_t brightness) {
    static uint8_t rainbowColor = 0;
    static uint8_t nextColor = 0;
    int thisColor[LIGHTS_PER_STRING] = {rainbowHue(rainbowColor, brightness)};
    
    for(int a = 1; a < LIGHTS_PER_STRING; a++) {
        nextColor = rainbowColor + a*LIGHTS_PER_STRING;
        thisColor[a] = rainbowHue(nextColor, brightness);
    }
    for(int i = 0; i < LIGHTS_PER_STRING; i++) {
        for(int j = 0; j < LIGHTS_PER_STRING; j++)
            LED[i*LIGHTS_PER_STRING + LIGHTS_PER_STRING - j] = thisColor[(LIGHTS_PER_STRING + i - j)%LIGHTS_PER_STRING];
    }
    rainbowColor++;
}
