// main.c
// Travis Llado
// 2016.05.26

////////////////////////////////////////////////////////////////////////////////
// Dependencies

#include <stdint.h>

#include "lightctrl.h"
#include "buttons.h"
#include "config.h"
#include "leds.h"
#include "PLL.h"
#include "rain.h"
#include "rainbows.h"
#include "stars.h"

////////////////////////////////////////////////////////////////////////////////
// Global Variables

const uint8_t numPrograms = 3;
uint8_t programNum = numPrograms;
uint8_t brightness = START_BRIGHTNESS;
uint32_t LED[NUM_STRINGS*LIGHTS_PER_STRING] = {0};

////////////////////////////////////////////////////////////////////////////////
// Internal Prototypes

void clearLEDs(void);
void leftButtonTask(void);
void rightButtonTask(void);
void visualizations(void);
void wait(uint32_t length);

////////////////////////////////////////////////////////////////////////////////
// main()
// Outermost structure of chandelier program. Initializes all hardware, then
// spins in a while loop forever as all further actions are performed by
// interrupt handlers.

int main(void) {
    PLL_Init();
    lightsInit(&visualizations, LED);
    buttonsInit(&leftButtonTask, &rightButtonTask);
    LEDsInit();
    redLEDOn();
    wait(1000);
    redLEDOff();
    programNum = 0; 
    
    while(1) {
        // NOTHING HERE!
        // Everything is done using timer interrupt handlers
    }
}

////////////////////////////////////////////////////////////////////////////////
// clearLEDs()
// Sets all LEDs to be off on next update.

void clearLEDs(void) {
    for(int i = 0; i < NUM_STRINGS*LIGHTS_PER_STRING; i++)
        LED[i] = 0;
}

////////////////////////////////////////////////////////////////////////////////
// leftButtonTask()
// Actions performed when left button is pressed

void leftButtonTask(void) {
    brightness = (brightness + BRIGHT_INCREMENT)%MAX_BRIGHTNESS;
    greenLEDToggle();
}

////////////////////////////////////////////////////////////////////////////////
// rightButtonTask()
// Actions performed when right button is pressed

void rightButtonTask(void) {
    programNum = (programNum + 1)%(numPrograms + 1);
    blueLEDToggle();
}

////////////////////////////////////////////////////////////////////////////////
// visualizations()
// Code executed once per frame period. All visualizations should be selected
// and called from here.

void visualizations(void) {
    static uint32_t lastProgram;
    
    if(lastProgram != programNum)
        clearLEDs();
    else {
        switch(programNum) {
            case 0:
                updateStars(LED, brightness);
                break;
            case 1:
                updateRain(LED, brightness);
                break;
            case 2:
                updateRainbow(LED, brightness);
                break;
            case 3:
                clearLEDs();
                break;
        }
    }
        
    lastProgram = programNum;
}

////////////////////////////////////////////////////////////////////////////////
// wait()
// Idles the CPU for desired time. Input desired time in milliseconds.

void wait(uint32_t length) {
    const uint32_t factor = 5400;   // converts ms to cycles
    const uint32_t interval = 100;  // ms
    volatile uint32_t num = 0;
    uint32_t numLoops = length/interval;
    
    blueLEDOn();
    
    for(uint32_t i = 0; i < numLoops; i++) {
        for(uint32_t j = 0; j < interval*factor; j++)
            num = 0;
        blueLEDToggle();
    }
    
    blueLEDOff();
}
