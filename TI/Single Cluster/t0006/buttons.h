// buttons.h
// Travis Llado
// 2016.05.26

////////////////////////////////////////////////////////////////////////////////
// buttonsInit()
// Initializes hardware and software associated with Launchpad's two onboard
// switches. Takes input of two function pointers that are referenced when
// GPIO Port F interrupt handler is executed.

void buttonsInit(void (*leftTask)(void), void (*rightTask)(void));
