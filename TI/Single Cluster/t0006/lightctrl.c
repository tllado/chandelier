// lightctrl.c
// Travis Llado
// 2016.05.26

////////////////////////////////////////////////////////////////////////////////
// Dependencies

#include <stdint.h>

#include "config.h"
#include "lightctrl.h"
#include "tm4c123gh6pm.h"

void DisableInterrupts(void);
void EnableInterrupts(void);
long StartCritical (void);
void EndCritical(long sr);
void WaitForInterrupt(void);

uint32_t bits[SIGNAL_LENGTH] = {0};
uint32_t bitCount = 0;
void (*updateTask)(void);
uint32_t *LEDs;

////////////////////////////////////////////////////////////////////////////////
// Internal Prototypes

void PortA_Init(void);
void PortB_Init(void);
void Timer1_Init(void);
void Timer1A_Handler(void);
void Timer2_Init(void);
void Timer2A_Handler(void);
void transformLights(void);
void updateLights(void);

////////////////////////////////////////////////////////////////////////////////
// lightsInit()
// Performs all hardware setup for chandelier light string cluster. Takes input
// of function to be performed each frame.

void lightsInit(void (*newFunc)(void), uint32_t *newArray) {
    updateTask = newFunc;
    LEDs = newArray;
     
    PortA_Init();
    PortB_Init();
    Timer1_Init();
    Timer2_Init();
}

////////////////////////////////////////////////////////////////////////////////
// PortA_Init()
// Initializes all hardware needed to use Port A GPIO pins

void PortA_Init(void) {
    DisableInterrupts();
        SYSCTL_RCGCGPIO_R |= 0x01;              // activate clock for Port A
        while((SYSCTL_PRGPIO_R&0x01) == 0) {}   // wait for clock to start
        GPIO_PORTA_AMSEL_R = 0x00;              // disable analog on PA0-7
        GPIO_PORTA_PCTL_R = 0x00;               // PCTL GPIO on PA0-7
        GPIO_PORTA_DIR_R = 0xFF;                // PA0-7 out
        GPIO_PORTA_AFSEL_R = 0x00;              // disable alt funct on PA0-7
        GPIO_PORTA_DEN_R = 0xFF;                // enable digital I/O on PA0-7
    EnableInterrupts();
}

////////////////////////////////////////////////////////////////////////////////
// PortB_Init()
// Initializes all hardware needed to use Port B GPIO pins

void PortB_Init(void) {
    DisableInterrupts();
        SYSCTL_RCGCGPIO_R |= 0x02;              // activate clock for Port B
        while((SYSCTL_PRGPIO_R&0x02) == 0) {}   // wait for clock to start
        GPIO_PORTB_AMSEL_R = 0x00;              // disable analog on PB0-7
        GPIO_PORTB_PCTL_R = 0x00;               // PCTL GPIO on PB0-7
        GPIO_PORTB_DIR_R = 0xFF;                // PB0-7 out
        GPIO_PORTB_AFSEL_R = 0x00;              // disable alt funct on PB0-7
        GPIO_PORTB_DEN_R = 0xFF;                // enable digital I/O on PB0-7
    EnableInterrupts();
}

////////////////////////////////////////////////////////////////////////////////
// Timer1_Init()
// Initializes all hardware needed to use Timer 1A for frame update

void Timer1_Init(void) {
    DisableInterrupts();
        SYSCTL_RCGCTIMER_R |= 0x02;         // activate TIMER1
        while(SYSCTL_RCGCTIMER_R == 0) {}   // wait for timer to start
        TIMER1_CTL_R = 0x00;                // disable TIMER1A during setup
        TIMER1_CFG_R = 0x00;                // set to 32-bit mode
        TIMER1_TAMR_R = 0x02;               // set to periodic mode
        TIMER1_TAILR_R = FRAME_PERIOD - 1;  // set reset value
        TIMER1_TAPR_R = 0;                  // set bus clock resolution
        TIMER1_ICR_R = 0x01;                // clear TIMER1A timeout flag
        TIMER1_IMR_R |= 0x01;               // arm timeout interrupt
        NVIC_PRI5_R = (NVIC_PRI5_R & 0x00FFFFFF) | (FRAME_PRIORITY << 29);
                                            // set priority
        NVIC_EN0_R = 1 << 21;               // enable IRQ 21 in NVIC
        TIMER1_CTL_R = 0x01;                // enable TIMER1A
    EnableInterrupts();
}

////////////////////////////////////////////////////////////////////////////////
// Starts sequence of visualization and LED update for a single frame.

void Timer1A_Handler(void) {
    TIMER1_ICR_R = 0x01;    // acknowledge timer1A timeout

    updateTask();
    updateLights();
}

////////////////////////////////////////////////////////////////////////////////
// Timer2_Init()
// Initializes all hardware needed to use Timer2A for bit updates

void Timer2_Init(void) {
    DisableInterrupts();
        SYSCTL_RCGCTIMER_R |= 0x04;         // activate timer2
        while(SYSCTL_RCGCTIMER_R == 0) {}   // wait for timer to start
        TIMER2_CTL_R = 0x00;                // disable TIMER2A during setup
        TIMER2_CFG_R = 0x00;                // set to 32-bit mode
        TIMER2_TAMR_R = 0x02;               // set to periodic mode
        TIMER2_TAILR_R = BIT_PERIOD - 1;    // set reset value
        TIMER2_TAPR_R = 0;                  // set bus clock resolution
        TIMER2_ICR_R = 0x01;                // clear TIMER1A timeout flag
        TIMER2_IMR_R |= 0x01;               // arm timeout interrupt
        NVIC_PRI5_R = (NVIC_PRI5_R & 0x00FFFFFF) | (BIT_PRIORITY << 29);
                                            // set priority
        NVIC_EN0_R = 1 << 23;               // enable interrupt 23 in NVIC
    EnableInterrupts();
}

////////////////////////////////////////////////////////////////////////////////
// Timer2A_Handler()
// Writes single bit to all LED strings

void Timer2A_Handler(void) {
    uint32_t volatile delay = 0;

    TIMER2_ICR_R = 0x01;                        // acknowledge timer2A timeout
    
    GPIO_PORTA_DATA_R = 0xFF;                   // write all PortA pins high
    delay=0;delay=0;delay=0;
    GPIO_PORTB_DATA_R = 0xFF;                   // write all PortB pins high
    GPIO_PORTA_DATA_R = bits[bitCount];         // write PortA to message bits
    GPIO_PORTB_DATA_R = bits[bitCount] >> 8;    // write PortB to message bits
    delay=0;delay=0;delay=0;delay=0;delay=0;    // delay remaining 1 time
    GPIO_PORTA_DATA_R = 0x00;                   // write all PortA pins low
    delay=0;delay=0;delay=0;
    GPIO_PORTB_DATA_R = 0x00;                   // write all PortB pins low

    bitCount--;
    if(bitCount == 0)                           // after entire message is sent
        TIMER2_CTL_R = 0x00;                    // disable Timer 2A
}

////////////////////////////////////////////////////////////////////////////////
// transformLights()
// Transforms array of 256 LED color values into array of (16*24) parallel GPIO
// values

void transformLights(void) {
    int bitIndex = 0;
    for(int lightNum = 0; lightNum < LIGHTS_PER_STRING; lightNum++) {
        for(int bitNum = 0; bitNum < BITS_PER_LIGHT; bitNum++) {
            bitIndex = lightNum*BITS_PER_LIGHT + bitNum;
            bits[bitIndex] = 0;
            for(int stringNum = 0; stringNum < NUM_STRINGS; stringNum++) {
                bits[bitIndex] += ((LEDs[stringNum*LIGHTS_PER_STRING           \
                    + lightNum] & (1 << bitNum)) >> bitNum) << stringNum;
            }
        }
    }
}

////////////////////////////////////////////////////////////////////////////////
// updateLights()
// Performs actions to commence writing of current contents of LED[] to LEDs.

void updateLights(void) {
    transformLights();
    bitCount = SIGNAL_LENGTH - 1;       // reset message bit counter
    TIMER2_CTL_R = 0x01;                // enable Timer 2A
}
