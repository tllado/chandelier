// stars.c
// Travis Llado
// 2016.05.26

////////////////////////////////////////////////////////////////////////////////
// Dependencies

#include <cstdlib>
#include <stdint.h>

#include "config.h"
#include "stars.h"
#include "tempcoloruint.h"

////////////////////////////////////////////////////////////////////////////////
// updateStars()
// Produces starfield visualization. Stars twinkle on and with random position,
//  brightness, and period.

void updateStars(uint32_t *LED, uint8_t brightness) {
    static uint32_t starCounter = 0;
    
    if(starCounter == STAR_PERIOD) {
        starCounter = 0;
        static uint32_t star[NUM_STARS*NUM_VARS] = {0};
        const uint32_t stringNum = 0;
        const uint32_t lightNum = 1;
        const uint32_t length = 2;
        const uint32_t bright = 3;
        const uint32_t color = 4;
        const uint32_t current = 5;
        uint32_t thisBrightness;
        
        for(int i = 0; i < NUM_STARS; i++) {
            // reinitialize single star
            if(star[i*NUM_VARS + current] == 0) {
                LED[star[i*NUM_VARS + stringNum]*LIGHTS_PER_STRING             \
                    + star[i*NUM_VARS + lightNum]] = 0;
                        
                star[i*NUM_VARS + stringNum] = rand()%(NUM_STRINGS - 1);
                star[i*NUM_VARS + lightNum] = rand()%(LIGHTS_PER_STRING - 1);
                star[i*NUM_VARS + length] = rand()%(MAX_LENGTH - MIN_LENGTH)   \
                    + MIN_LENGTH;
                star[i*NUM_VARS + bright] = rand()%(MAX_BRIGHT - MIN_BRIGHT)   \
                    + MIN_BRIGHT;
                star[i*NUM_VARS + color] = rand()%255;
                star[i*NUM_VARS + current] = star[i*NUM_VARS + length];
            }

            // update star brightness
            if(star[i*NUM_VARS + current] > star[i*NUM_VARS + length]/2)
                thisBrightness = brightness * star[i*NUM_VARS + bright]/255 *  \
                    (star[i*NUM_VARS + length] - star[i*NUM_VARS               \
                    + current])/(star[i*NUM_VARS + length]/2);
            else
                thisBrightness = brightness * star[i*NUM_VARS + bright]/255 *  \
                    star[i*NUM_VARS + current]/(star[i*NUM_VARS + length]/2);
            star[i*NUM_VARS + current]--;

            // write new brightness value to LED array
            LED[star[i*NUM_VARS + stringNum]*LIGHTS_PER_STRING                 \
                + star[i*NUM_VARS + lightNum]] =                               \
                tempColor(MIN_COLOR + (MAX_COLOR - MIN_COLOR)*                 \
                star[i*NUM_VARS + color]/255, thisBrightness);
        }
    }
    starCounter++;
}
