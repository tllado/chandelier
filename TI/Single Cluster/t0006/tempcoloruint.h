// tempcoloruint.h

////////////////////////////////////////////////////////////////////////////////
// tempColor()
// Accepts two values (for temperature and brightness) and returns the
// corresponding neopixel color code (uint32_t). temp=0 is full red (1000K),
// temp=128 is neutral (6600K), and temp=255 is full blue (40000K).

uint32_t tempColor(uint8_t temperature, uint8_t brightness);
