// buttons.c
// Travis Llado
// 2016.05.26

////////////////////////////////////////////////////////////////////////////////
// Dependencies

#include <stdint.h>

#include "buttons.h"
#include "tm4c123gh6pm.h"

void DisableInterrupts(void);
void EnableInterrupts(void);
long StartCritical (void);
void EndCritical(long sr);
void WaitForInterrupt(void);

////////////////////////////////////////////////////////////////////////////////
// Global Variables

#define SW1   0x10
#define SW2   0x01
void (*SW1Task)(void);
void (*SW2Task)(void);

////////////////////////////////////////////////////////////////////////////////
// Internal Prototypes

void buttonsInit(void (*leftTask)(void), void (*rightTask)(void));
void debounce(void);
void GPIOPortF_Handler(void);
void SW1_Init(void);
void SW2_Init(void);

////////////////////////////////////////////////////////////////////////////////
// buttonsInit()
// Initializes hardware and software associated with Launchpad's two onboard
// switches. Takes input of two function pointers that are referenced when
// GPIO Port F interrupt handler is executed.

void buttonsInit(void (*leftTask)(void), void (*rightTask)(void)) {
    SW1_Init();
    SW2_Init();
    SW1Task = leftTask;
    SW2Task = rightTask;
}

////////////////////////////////////////////////////////////////////////////////
// GPIOPortF_Handler()
// A very primitive debounce function. Just wastes some time before clearing
// button interrupt.

void debounce(void) {
    volatile uint32_t delay = 0;
    const uint32_t length = 1000000;
    
    for(uint32_t i = 0; i < length; i++)
        delay = 0;
}

////////////////////////////////////////////////////////////////////////////////
// GPIOPortF_Handler()
// Decides what action to take for any interrupt on Port F. This includes the
// Launchpad's two onboard switches.

void GPIOPortF_Handler(void) {
    if(GPIO_PORTF_RIS_R & 0x10) {   // poll PF4(SW1)
        debounce();
        GPIO_PORTF_ICR_R = 0x10;    // acknowledge flag4
        SW1Task();
    }
    if(GPIO_PORTF_RIS_R & 0x01) {   // poll PF0(SW2)
        debounce();
        GPIO_PORTF_ICR_R = 0x01;    // acknowledge flag0
        SW2Task();
    }
}

////////////////////////////////////////////////////////////////////////////////
// SW1_Init()
// Initializes Switch 1.
// Input: none
// Output: none

void SW1_Init() {
    DisableInterrupts();
    SYSCTL_RCGCGPIO_R |= 0x20;              // activate clock for port F   
    while((SYSCTL_PRGPIO_R&0x20) == 0) {}   // wait for clock to stabilize
    GPIO_PORTF_DIR_R &= ~0x10;              // make PF4 in (built-in button)
    GPIO_PORTF_AFSEL_R &= ~0x10;            // disable alt funct on PF4
    GPIO_PORTF_DEN_R |= 0x10;               // enable digital I/O on PF4   
    GPIO_PORTF_PCTL_R &= ~0x000F0000;       // configure PF4 as GPIO
    GPIO_PORTF_AMSEL_R &= ~0x10;            // disable analog functions on PF4
    GPIO_PORTF_PUR_R |= 0x10;               // enable weak pull-up on PF4
    GPIO_PORTF_IS_R &= ~0x10;               // PF4 is edge-sensitive
    GPIO_PORTF_IBE_R &= ~0x10;              // PF4 is not both edges
    GPIO_PORTF_IEV_R &= ~0x10;              // PF4 falling edge event
    GPIO_PORTF_ICR_R = 0x10;                // clear flag4
    GPIO_PORTF_IM_R |= 0x10;                // arm interrupt on PF4 
    NVIC_PRI7_R &= 0xFF00FFFF;              // priority 0
    NVIC_PRI7_R |= 0x00;                    // priority 0
    NVIC_EN0_R = 0x40000000;                // enable interrupt 30 in NVIC
    EnableInterrupts();
}

////////////////////////////////////////////////////////////////////////////////
// SW2_Init()
// Initializes Switch 2.
// Input: none
// Output: none

void SW2_Init() {
    DisableInterrupts();
    SYSCTL_RCGCGPIO_R |= 0x20;              // activate clock for port F   
    while((SYSCTL_PRGPIO_R&0x20) == 0) {}   // wait for clock to stabilize
    GPIO_PORTF_LOCK_R = GPIO_LOCK_KEY;      // unlock GPIO Port F Commit reg
    GPIO_PORTF_CR_R |= (SW1|SW2);           // enable commit for PF4 and PF0
    GPIO_PORTF_DIR_R &= ~0x01;              // make PF0 in (built-in button)
    GPIO_PORTF_AFSEL_R &= ~0x01;            // disable alt funct on PF0
    GPIO_PORTF_DEN_R |= 0x01;               // enable digital I/O on PF0   
    GPIO_PORTF_PCTL_R &= ~0x0F;             // configure PF0 as GPIO
    GPIO_PORTF_AMSEL_R &= ~0x01;            // disable analog functions on PF0
    GPIO_PORTF_PUR_R |= 0x01;               // enable weak pull-up on PF0
    GPIO_PORTF_IS_R &= ~0x01;               // PF0 is edge-sensitive
    GPIO_PORTF_IBE_R &= ~0x01;              // PF0 is not both edges
    GPIO_PORTF_IEV_R &= ~0x01;              // PF0 falling edge event
    GPIO_PORTF_ICR_R = 0x01;                // clear flag0
    GPIO_PORTF_IM_R |= 0x01;                // arm interrupt on PF0 
    NVIC_PRI7_R &= 0xFF00FFFF;              // priority 0
    NVIC_PRI7_R |= 0x00;                    // priority 0
    NVIC_EN0_R = 0x40000000;                // enable interrupt 30 in NVIC
    DisableInterrupts();
}
