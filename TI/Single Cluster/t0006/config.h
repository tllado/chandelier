// config.h
// Travis Llado
// 2016.05.26

////////////////////////////////////////////////////////////////////////////////
// Visualization Settings

// System
#define MAX_BRIGHTNESS      90
#define START_BRIGHTNESS    25
#define BRIGHT_INCREMENT    10
// Stars
#define STAR_PERIOD 1
#define NUM_STARS   50
#define NUM_VARS    6
#define MIN_COLOR   80
#define MAX_COLOR   165
#define MIN_LENGTH  50
#define MAX_LENGTH  250
#define MIN_BRIGHT  60
#define MAX_BRIGHT  255
// Rain
#define RAIN_PERIOD 3
#define RAIN_COLOR  250
#define RAIN_PROB   15

////////////////////////////////////////////////////////////////////////////////
// Hardware Settings

#define FRAMERATE           60          // Hz
#define BIT_FREQ            800000      // Hz
#define CLOCK_FREQ          80000000    // Hz
#define BITS_PER_LIGHT      24
#define LIGHTS_PER_STRING   16
#define NUM_STRINGS         16
#define BIT_PRIORITY        1
#define FRAME_PRIORITY      2
#define BIT_PERIOD          CLOCK_FREQ/BIT_FREQ
#define FRAME_PERIOD        CLOCK_FREQ/FRAMERATE
#define SIGNAL_LENGTH       LIGHTS_PER_STRING*BITS_PER_LIGHT
