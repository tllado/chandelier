// boardsetup.h

////////////////////////////////////////////////////////////////////////////////
// Dependencies

#include <stdint.h>
#include "tm4c123gh6pm.h"
#include "PLL.h"

void DisableInterrupts(void);
void EnableInterrupts(void);
long StartCritical (void);
void EndCritical(long sr);
void WaitForInterrupt(void);

////////////////////////////////////////////////////////////////////////////////
// User Settings

#define FRAMERATE           60          // Hz
#define BIT_FREQ            800000      // Hz
#define CLOCK_FREQ          80000000    // Hz
#define BITS_PER_LIGHT      24
#define LIGHTS_PER_STRING   16
#define NUM_STRINGS         16
#define BIT_PRIORITY        1
#define FRAME_PRIORITY      2

#define BIT_PERIOD          CLOCK_FREQ/BIT_FREQ
#define FRAME_PERIOD        CLOCK_FREQ/FRAMERATE
#define SIGNAL_LENGTH       LIGHTS_PER_STRING*BITS_PER_LIGHT

////////////////////////////////////////////////////////////////////////////////
// Exported Functions

void PortA_Init(void);
void PortB_Init(void);
void PortD_Init(void);
int Timer1_Init(void);
int Timer2_Init(void);
void Timer1A_Handler(void);
void Timer2A_Handler(void);
