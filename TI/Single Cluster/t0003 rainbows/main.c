#include "tempcolor.h"
#include "boardsetup.h"

int main(void){
  	PLL_Init();
  	PortA_Init();
  	PortB_Init();
    PortD_Init();
	Timer1_Init();
	Timer2_Init();

    LED_Power(1);
  
	while(1){
    // NOTHING HERE!
		// Everything is done using timer interrupt handlers
  }
}
