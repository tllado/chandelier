// boardsetup.c

////////////////////////////////////////////////////////////////////////////////
// Dependencies

#include "boardsetup.h"

////////////////////////////////////////////////////////////////////////////////
// Global Variables

#define PD0         (*((volatile uint32_t *)0x40007004))
#define PD0_HIGH    0xFF
#define PDO_LOW     0x00

int LED[NUM_STRINGS][LIGHTS_PER_STRING] = {0};
int bits[SIGNAL_LENGTH] = {0};
int volatile delay = 0;
int bitCount = 0;

////////////////////////////////////////////////////////////////////////////////
// Function Prototypes

void PortA_Init(void);
void PortB_Init(void);
int Timer1_Init(void);
int Timer2_Init(void);
void Timer1A_Handler(void);
void Timer2A_Handler(void);
void LEDPower(uint8_t);
void transformLights(void);
int rainbow(int);
void makeAFewColors(void);

////////////////////////////////////////////////////////////////////////////////

void PortA_Init(void) {
    DisableInterrupts();
        SYSCTL_RCGC2_R |= 0x01;         // activate clock for Port A
        while((SYSCTL_PRGPIO_R&0x01) == 0) {}
                                        // allow time for clock to start
        GPIO_PORTA_AMSEL_R = 0x00;      // disable analog on PA0-7
        GPIO_PORTA_PCTL_R = 0x00;       // PCTL GPIO on PA0-7
        GPIO_PORTA_DIR_R = 0xFF;        // PA0-7 out
        GPIO_PORTA_AFSEL_R = 0x00;      // disable alt funct on PA0-7
        GPIO_PORTA_DEN_R = 0xFF;        // enable digital I/O on PA0-7
    EnableInterrupts();
}

////////////////////////////////////////////////////////////////////////////////

void PortB_Init(void) {
    DisableInterrupts();
        SYSCTL_RCGC2_R |= 0x02;         // activate clock for Port B
        while((SYSCTL_PRGPIO_R&0x02) == 0) {}
                                        // wait for clock to start
        GPIO_PORTB_AMSEL_R = 0x00;      // disable analog on PB0-7
        GPIO_PORTB_PCTL_R = 0x00;       // PCTL GPIO on PB0-7
        GPIO_PORTB_DIR_R = 0xFF;        // PB0-7 out
        GPIO_PORTB_AFSEL_R = 0x00;      // disable alt funct on PB0-7
        GPIO_PORTB_DEN_R = 0xFF;        // enable digital I/O on PB0-7
    EnableInterrupts();
}

////////////////////////////////////////////////////////////////////////////////

void PortD_Init(void) {
    DisableInterrupts();
        SYSCTL_RCGC2_R |= 0x08;         // activate clock for Port D
        while((SYSCTL_PRGPIO_R&0x08) == 0) {}
                                        // wait for clock to start
        GPIO_PORTD_AMSEL_R & ~0x01;     // disable analog on PD0
        GPIO_PORTD_PCTL_R & ~0x01;      // PCTL GPIO on PD0
        GPIO_PORTD_DIR_R = 0x01;        // PD0 out
        GPIO_PORTD_AFSEL_R & ~0x01;     // disable alt funct on PD0
        GPIO_PORTD_DEN_R = 0x01;        // enable digital I/O on PD0
    EnableInterrupts();
}

////////////////////////////////////////////////////////////////////////////////

int Timer1_Init(void) {
    DisableInterrupts();
        SYSCTL_RCGCTIMER_R |= 0x02;         // activate TIMER1
        while(SYSCTL_RCGCTIMER_R == 0) {}   // wait for timer to start
        TIMER1_CTL_R = 0x00;                // disable TIMER1A during setup
        TIMER1_CFG_R = 0x00;                // set to 32-bit mode
        TIMER1_TAMR_R = 0x02;               // set to periodic mode
        TIMER1_TAILR_R = framePeriod - 1;   // set reset value
        TIMER1_TAPR_R = 0;                  // set bus clock resolution
        TIMER1_ICR_R = 0x01;                // clear TIMER1A timeout flag
        TIMER1_IMR_R |= 0x01;               // arm timeout interrupt
        NVIC_PRI5_R = (NVIC_PRI5_R & 0x00FFFFFF) | (framePriority << 29);
                                            // set priority
        NVIC_EN0_R = 1 << 21;               // enable IRQ 21 in NVIC
        TIMER1_CTL_R = 0x01;                // enable TIMER1A
    EnableInterrupts();
  return 1;
}

////////////////////////////////////////////////////////////////////////////////

int Timer2_Init(void) {
    DisableInterrupts();
        SYSCTL_RCGCTIMER_R |= 0x04;         // activate timer2
        while(SYSCTL_RCGCTIMER_R == 0) {}   // wait for timer to start
        TIMER2_CTL_R = 0x00;                // disable TIMER2A during setup
        TIMER2_CFG_R = 0x00;                // set to 32-bit mode
        TIMER2_TAMR_R = 0x02;               // set to periodic mode
        TIMER2_TAILR_R = bitPeriod - 1;     // set reset value
        TIMER2_TAPR_R = 0;                  // set bus clock resolution
        TIMER2_ICR_R = 0x01;                // clear TIMER1A timeout flag
        TIMER2_IMR_R |= 0x01;               // arm timeout interrupt
        NVIC_PRI5_R = (NVIC_PRI5_R & 0x00FFFFFF) | (bitPriority << 29);
                                            // set priority
        NVIC_EN0_R = 1 << 23;               // enable interrupt 23 in NVIC
    EnableInterrupts();
    return 1;
}

////////////////////////////////////////////////////////////////////////////////

void Timer1A_Handler(void) {
    TIMER1_ICR_R = 0x01;            // acknowledge timer1A timeout
    makeAFewColors();
    transformLights();              // transform LED arrays to bit arrays
    bitCount = signalLength - 1;    // reset message bit counter
    TIMER2_CTL_R = 0x01;            // enable Timer 2A
}

////////////////////////////////////////////////////////////////////////////////

void Timer2A_Handler(void) {
    TIMER2_ICR_R = 0x01;                        // acknowledge timer2A timeout
    
    GPIO_PORTA_DATA_R = 0xFF;                   // write all PortA pins high
    delay=0;delay=0;delay=0;
    GPIO_PORTB_DATA_R = 0xFF;                   // write all PortB pins high
    GPIO_PORTA_DATA_R = bits[bitCount];         // write PortA to message bits
    GPIO_PORTB_DATA_R = bits[bitCount] >> 8;    // write PortB to message bits
    delay=0;delay=0;delay=0;delay=0;delay=0;    // delay remaining 1 time
    GPIO_PORTA_DATA_R = 0x00;                   // write all PortA pins low
    delay=0;delay=0;delay=0;
    GPIO_PORTB_DATA_R = 0x00;                   // write all PortB pins low

    bitCount--;
    if(bitCount == 0)                           // after entire message is sent
        TIMER2_CTL_R = 0x00;                    // disable Timer 2A
}

////////////////////////////////////////////////////////////////////////////////

void LEDPower(uint8_t status) {
    if(!status)
        PD0 = PD0_HIGH;
    else
        PD0 = PD0_LOW;
    
    return;
}

////////////////////////////////////////////////////////////////////////////////

// Transform next frame array of LED values to array of parallel bus bytes
void transformLights(void) {
    int bitIndex = 0;
    for(int lightNum = 0; lightNum < lightsPerString; lightNum++) {
        for(int bitNum = 0; bitNum < bitsPerLight; bitNum++) {
            bitIndex = lightNum*bitsPerLight + bitNum;
            bits[bitIndex] = 0;
            for(int stringNum = 0; stringNum < numStrings; stringNum++) {
                bits[bitIndex] += ((LED[stringNum][lightNum] &
                    (1 << bitNum)) >> bitNum) << stringNum;
            }
        }
    }
}

////////////////////////////////////////////////////////////////////////////////

// Create 24-bit color code from 8-bit hue
int rainbow(int hue) {
    hue = 255 - hue;
    if(hue < 85)
        return ((255 - hue*3) << 16) + (hue*3);
    if(hue < 170) {
        hue -= 85;
        return ((hue*3) << 8) + (255 - hue*3);
    }
    hue -= 170;
    return ((hue*3) << 16) + ((255 - hue*3) << 8);
}  

////////////////////////////////////////////////////////////////////////////////

// Temporary function, write rainbow colors to all ports
uint8_t rainbowColor = 0;
uint8_t nextColor = 0;
void makeAFewColors(void) {
    const int magicNum = 16;
    int thisColor[magicNum] = {rainbow(rainbowColor)};
    for(int a = 1; a < magicNum; a++) {
        nextColor = rainbowColor + a*magicNum;
        thisColor[a] = rainbow(nextColor);
    }
    for(int i = 0; i < magicNum; i++) {
        for(int j = 0; j < magicNum; j++) {
            LED[i][j] = thisColor[(magicNum + i - j)%magicNum];
        }
    }
    rainbowColor++;
}
