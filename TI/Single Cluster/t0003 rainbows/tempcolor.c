// tempcolor.c

#include "tempcolor.h"

// color
// Accepts two values (for temperature and brightness) and
// returns the corresponding neopixel color code (32b int)
uint32_t tempColor(double T, double brite) {
    uint8_t Red = R(T)*brite;
    uint8_t Grn = G(T)*brite;
    uint8_t Blu = B(T)*brite;

    return (uint32_t)Grn << 16 | (uint32_t)Red <<  8 | Blu;
}

// R
// Generates approximate red light values for black body
// radiation at T Kelvin using a 3rd order polynomial
uint8_t R(double T) {
    if(T >= -1.0) {
        if(T <= 0.0)
            return 255*(1);
    if(T <= 1.0)
        return 255*(-0.990*T*T*T + 2.34*T*T - 1.99*T + 0.970);
    }
    return 0;
}

// G
// Generates approximate grenn light values for black body
// radiation at T Kelvin using a 3rd order polynomial
uint8_t G(double T) {
    if(T >= -1.0) {
        if(T <= 0.0)
            return 255*(-0.402*T*T*T - 0.211*T*T + 1.09*T + 0.958);
    if(T <= 1.0)
        return 255*(-0.542*T*T*T + 1.37*T*T - 1.28*T + 0.941);
    }
    return 0;
}

// B
// Generates approximate blue light values for black body
// radiation at T Kelvin using a 3rd order polynomial
uint8_t B(double T) {
    if(T >= -1.0) {
        if(T <= -0.7)
            return 255*(0);
        if(T <= 0.0)
            return 255*(0.0117*T*T*T + 2.05*T*T + 2.85*T + 1.00);
        if(T <= 1.0)
            return 255*(1);
    }
    return 0;
}
