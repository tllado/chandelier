// boardsetup.c

#include <stdint.h>
#include "tm4c123gh6pm.h"
#include "PLL.h"
#include "boardsetup.h"

void DisableInterrupts(void);
void EnableInterrupts(void);
long StartCritical (void);
void EndCritical(long sr);
void WaitForInterrupt(void);

#define PF2 (*((volatile unsigned long *)0x40025010))

const int bitPeriod = 100;          // 80MHz/800KHz = 100 cycles
const int framePeriod = 1333333;    // 80MHz/60fps = 1.333M cycles
const int bitPriority = 2;
const int framePriority = 3;

unsigned long volatile delay = 0;
int counter1 = 0;
int counter2 = 0;
int color = 0xF0F0F000;	// last 24 bits of this word will be transmitted repeatedly
int nextBit = 0;
int oneCycle = 0;

void PF2_Init(void) {
    SYSCTL_RCGCGPIO_R |= 0x20;                  // activate port F
    while((SYSCTL_PRGPIO_R & 0x20) == 0){};
    GPIO_PORTF_DIR_R |= 0x04;                   // set PF2 to output
    GPIO_PORTF_AFSEL_R &= ~0x04;                // disable alternative functions for PF2
    GPIO_PORTF_DEN_R |= 0x04;                   // enable digital port for PF2
    GPIO_PORTF_PCTL_R = GPIO_PORTF_PCTL_R & 0xFFFF000F; // GPIO
    GPIO_PORTF_AMSEL_R = 0;                     // disable analog functionality on PF
}

int Timer1_Init() {
    DisableInterrupts();
        SYSCTL_RCGCTIMER_R |= 0x02;     // activate TIMER1
        delay = SYSCTL_RCGCTIMER_R;
        TIMER1_CTL_R = 0x00000000;      // disable TIMER1A during setup
        TIMER1_CFG_R = 0x00000000;      // set to 32-bit mode
        TIMER1_TAMR_R = 0x00000002;     // set to periodic mode
        TIMER1_TAILR_R = framePeriod - 1; // set reset value
        TIMER1_TAPR_R = 0;              // set bus clock resolution
        TIMER1_ICR_R = 0x00000001;      // clear TIMER1A timeout flag
        TIMER1_IMR_R |= 0x00000001;     // arm timeout interrupt
        NVIC_PRI5_R = (NVIC_PRI5_R & 0x00FFFFFF) | (bitPriority << 29); // set priority
        NVIC_EN0_R = 1 << 21;           // enable IRQ 21 in NVIC
        TIMER1_CTL_R = 0x00000001;      // enable TIMER1A
    EnableInterrupts();
  return 1;
}

int Timer2_Init() {
    DisableInterrupts();
        SYSCTL_RCGCTIMER_R |= 0x04;         // activate timer2
        delay = SYSCTL_RCGCTIMER_R;
        TIMER2_CTL_R = 0x00000000;          // disable TIMER2A during setup
        TIMER2_CFG_R = 0x00000000;          // set to 32-bit mode
        TIMER2_TAMR_R = 0x00000002;         // set to periodic mode
        TIMER2_TAILR_R = bitPeriod - 1;   // set reset value
        TIMER2_TAPR_R = 0;                  // set bus clock resolution
        TIMER2_ICR_R = 0x00000001;          // clear TIMER1A timeout flag
        TIMER2_IMR_R |= 0x00000001;         // arm timeout interrupt
        NVIC_PRI5_R = (NVIC_PRI5_R & 0x00FFFFFF) | (framePriority << 29);   // set priority
        NVIC_EN0_R = 1 << 23;               // enable interrupt 23 in NVIC
        TIMER2_CTL_R |= 0x00000001;         // enable TIMER2A
    EnableInterrupts();
    return 1;
}

void Timer1A_Handler(void) {
    TIMER1_ICR_R = 0x00000001;// acknowledge timer0A timeout
    counter1 = 24;
    counter2 = 24;
}

void Timer2A_Handler(void) {
    TIMER2_ICR_R = 0x00000001;// acknowledge timer0A timeout
    if(counter1 > 0) {
        PF2 = 0x04; // write group 1 GPIO high
        counter1--;
        nextBit = (color & (1 << counter1)) >> counter1; // read next bit from buffer;
        waitZero(); // wait zero delay time
        PF2 = nextBit*4; // write all pins to stored value
        waitOne();  // wait remaining one delay time
        PF2 = 0x00; // write all pins low
        return;
    }
    // if(counter2 > 0) {
    //     PF2 = 0x04; // write group 1 GPIO high
    //     counter2--;
    //     nextBit = (color & (1 << counter2)) >> counter2; // read next bit from buffer;
    //     waitZero(); // wait zero delay time
    //     PF2 = nextBit; // write all pins to stored value
    //     waitOne();  // wait remaining one delay time
    //     PF2 = 0x00; // write all pins low
    //     return;
    // }
}

void waitZero() {
    oneCycle = 0;//oneCycle = 0;oneCycle = 0;oneCycle = 0;oneCycle = 0;oneCycle = 0;oneCycle = 0;oneCycle = 0;
    //oneCycle = 0;oneCycle = 0;oneCycle = 0;oneCycle = 0;
}

void waitOne() {
    oneCycle = 0;//oneCycle = 0;oneCycle = 0;oneCycle = 0;oneCycle = 0;oneCycle = 0;oneCycle = 0;oneCycle = 0;
    //voneCycle = 0;oneCycle = 0;oneCycle = 0;oneCycle = 0;
}