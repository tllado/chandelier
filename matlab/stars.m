function stars()
% stars twinkling in and out

% User settings
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
numstars=100;
size = 5;
lengthmin=1;
lengthmax=10;

elev=20;
rotspeed=15;
framerate=30;
movielength=900;
movietitle='../movies/stars.mp4';
movieformat='MPEG-4';
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    res=16;
    azim=0;
    x=1;
    y=2;
    z=3;
    time=4;
    length=5;
    axes=[1 res 1 res 1 res];
    movie = VideoWriter(movietitle,movieformat);
    open(movie);
    
    stars=[ceil(res*rand(numstars,3)) zeros(numstars,1) ceil(((lengthmax-lengthmin)*rand(numstars,1)+lengthmin)*framerate)];
    
    for a=1:movielength
        stars(:,time)=stars(:,time)+1;
        for i=1:numstars
            if (stars(i,time)==stars(i,length))
                stars(i,:)=[ceil(res*rand(1,3)) 0 ceil(((lengthmax-lengthmin)*rand+lengthmin)*framerate)];
            end
        end
        
        plot3(1,1,1);
        axis(axes);
        view(azim,elev);
        azim=azim+rotspeed/framerate;
        hold on;
        
        for i=1:numstars
            plot3(stars(i,x),stars(i,y),stars(i,z),'. b','MarkerSize',size*sin(pi*stars(i,time)/stars(i,length))+0.1);
        end
        
        hold off;
        writeVideo(movie,getframe);
    end
    close(movie);
end