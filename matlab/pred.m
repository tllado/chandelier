function pred()
% flock of prey avoid predators, predators chase prey, inside box

% User settings
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
preynum=150;
preyspeed=10;
preyspacing=1.5;
preysize=3;
flockgain=0.015;
velogain=0.2;
preyspacegain=0.1;
fleegain=10.0;
wallgain=0.1;
boundary=0.8;

prednum=3;
predspeed=15;
predspacing=3;
predpreyspacing=5;
predsize=10;
chasegain=0.003;
predspacegain=0.5;

elev=20;
rotspeed=15;
framerate=30;
movielength=900;
movietitle='../movies/pred.mp4';
movieformat='MPEG-4';
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    res=16;
    azim=0;
    x=1;
    y=2;
    z=3;
    xvel=4;
    yvel=5;
    zvel=6;
    axes=[1 res 1 res 1 res];
    movie = VideoWriter(movietitle,movieformat);
    open(movie);
    
    prey=[ceil(res*rand(preynum,3)) (2*rand(preynum,3)-1)*preyspeed/framerate];
    pred=[ceil(res*rand(prednum,3)) (2*rand(prednum,3)-1)*predspeed/framerate];
    
    for a=1:movielength
        flockpos=mean(prey(:,x:z));
        flockvel=mean(prey(:,xvel:zvel));
        
        % prey fly toward center of flock
        prey(:,xvel:zvel)=prey(:,xvel:zvel)+(repmat(flockpos,preynum,1)-prey(:,x:z))*flockgain;
        
        % match prey direction with flock
        prey(:,xvel:zvel)=prey(:,xvel:zvel)+repmat(flockvel,preynum,1)*velogain;
        
        for i=1:preynum
            % prey don't run into walls
            for j=x:z
                if (prey(i,j)>res*boundary)
                    prey(i,j+3)=prey(i,j+3)-(prey(i,j)-res*boundary)*wallgain;
                elseif (prey(i,j)<res*(1-boundary))
                    prey(i,j+3)=prey(i,j+3)+(res*(1-boundary)-prey(i,j))*wallgain;
                end
            end
            
            % maintain distance between prey
            for j=1:preynum
                if (hypot(prey(i,x)-prey(j,x),hypot(prey(i,y)-prey(j,y),prey(i,z)-prey(j,z)))<preyspacing)
                    prey(i,xvel:zvel)=prey(i,xvel:zvel)+(prey(i,x:z)-prey(j,x:z))*preyspacegain;
                end
            end
            
            % maintain prey distance from predators
            for j=1:prednum
                if (hypot(prey(i,x)-pred(j,x),hypot(prey(i,y)-pred(j,y),prey(i,z)-pred(j,z)))<predpreyspacing)
                    prey(i,xvel:zvel)=prey(i,xvel:zvel)+(prey(i,x:z)-pred(j,x:z))*fleegain;
                end
            end
            
            % limit prey speed
            prey(i,xvel:zvel)=prey(i,xvel:zvel)*preyspeed/framerate/hypot(prey(i,xvel),hypot(prey(i,yvel),prey(i,zvel)));
        end
        
        % pred fly toward center of flock
        pred(:,xvel:zvel)=pred(:,xvel:zvel)+(repmat(flockpos,prednum,1)-pred(:,x:z))*chasegain;
        
        for i=1:prednum
            % maintain distance between pred
            for j=1:prednum
                if (hypot(pred(i,x)-pred(j,z),hypot(pred(i,y)-pred(j,y),pred(i,z)-pred(j,z)))<predspacing)
                    pred(i,xvel:zvel)=pred(i,xvel:zvel)+(pred(i,x:z)-pred(j,x:z))*predspacegain;
                end
            end
            
            % limit pred speed
            pred(i,xvel:zvel)=pred(i,xvel:zvel)*predspeed/framerate/hypot(pred(i,xvel),hypot(pred(i,yvel),pred(i,zvel)));
        end
        
        prey(:,x:z)=prey(:,x:z)+prey(:,xvel:zvel);
        pred(:,x:z)=pred(:,x:z)+pred(:,xvel:zvel);
        
        plot3(1,1,1);
        axis(axes);
        view(azim,elev);
        azim=azim+rotspeed/framerate;
        hold on;
        
        plot3(ceil(prey(:,x)),ceil(prey(:,y)),ceil(prey(:,z)),'. b','MarkerSize',preysize);
        plot3(ceil(pred(:,x)),ceil(pred(:,y)),ceil(pred(:,z)),'. r','MarkerSize',predsize);
        
        hold off;
        writeVideo(movie,getframe);
    end
    close(movie);
end