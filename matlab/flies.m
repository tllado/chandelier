function flies()
% fireflies fly and flash randomly

% User settings
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
numflies=8;
speed=5;
size=6;
minperiod=2;
maxperiod=5;
flashlength=0.5;

randomgain=0.1;
wallgain=0.2;
boundary=0.8;

elev=20;
rotspeed=15;
framerate=30;
movielength=900;
movietitle='../movies/flies.mp4';
movieformat='MPEG-4';
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    res=16;
    azim=0;
    x=1;
    y=2;
    z=3;
    xvel=4;
    yvel=5;
    zvel=6;
    time=7;
    axes=[1 res 1 res 1 res];
    movie = VideoWriter(movietitle,movieformat);
    open(movie);
    
    flies=[ceil(res*rand(numflies,3)) (2*rand(numflies,3)-1)*speed/framerate ceil((rand(numflies,1)*(maxperiod-minperiod)+flashlength)*framerate)];
    
    for a=1:movielength
        flies(:,time)=flies(:,time)-1;
        
        plot3(1,1,1);
        axis(axes);
        view(azim,elev);
        azim=azim+rotspeed/framerate;
        hold on;
        
        % add random direction change
        flies(:,xvel:zvel)=flies(:,xvel:zvel)+(2*rand(numflies,3)-1)*randomgain;
        
        for i=1:numflies
            % flash fireflies
            if(flies(i,time)<flashlength*framerate)
                plot3(ceil(flies(i,x)),ceil(flies(i,y)),ceil(flies(i,z)),'. b','MarkerSize',size*sin(pi*flies(i,time)/flashlength/framerate)+0.1);
            end
            if(flies(i,time)==0)
                flies(i,time)=ceil((rand*(maxperiod-minperiod)+minperiod+flashlength)*framerate);
            end
            
            % fireflies don't run into walls
            for j=x:z
                if (flies(i,j)>res*boundary)
                    flies(i,j+3)=flies(i,j+3)-(flies(i,j)-res*boundary)*wallgain;
                elseif (flies(i,j)<res*(1-boundary))
                    flies(i,j+3)=flies(i,j+3)+(res*(1-boundary)-flies(i,j))*wallgain;
                end
            end
            
            % limit firefly speed
            flies(i,xvel:zvel)=flies(i,xvel:zvel)*speed/framerate/hypot(flies(i,xvel),hypot(flies(i,yvel),flies(i,zvel)));
        end
        
        % update positions
        flies(:,x:z)=flies(:,x:z)+flies(:,xvel:zvel);
        
        hold off;
        writeVideo(movie,getframe);
    end
    close(movie);
end