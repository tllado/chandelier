function prey()
% flock of prey flying randomly inside box

% User settings
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
preynum=100;
preyspeed=10;
preyspacing=1.5;
preysize=3;
flockgain=0.01;
velogain=0.1;
spacinggain=0.1;
wallgain=0.2;
boundary = 0.8;

elev=20;
rotspeed=15;
framerate=30;
movielength=900;
movietitle='../movies/prey.mp4';
movieformat='MPEG-4';
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    res=16;
    azim=0;
    x=1;
    y=2;
    z=3;
    xvel=4;
    yvel=5;
    zvel=6;
    axes=[1 res 1 res 1 res];
    movie = VideoWriter(movietitle,movieformat);
    open(movie);
    
    prey=[ceil(res*rand(preynum,3)) (2*rand(preynum,3)-1)*preyspeed/framerate];
    
    for a=1:movielength
        preycenter=mean(prey(:,x:z));
        preyvelo=mean(prey(:,xvel:zvel));
        
        % prey fly toward center of flock
        prey(:,xvel:zvel)=prey(:,xvel:zvel)+(repmat(preycenter,preynum,1)-prey(:,x:z))*flockgain;
        
        % match prey direction with flock
        prey(:,xvel:zvel)=prey(:,xvel:zvel)+repmat(preyvelo,preynum,1)*velogain;
        
        for i=1:preynum
            % prey don't run into walls
            for j=x:z
                if (prey(i,j)>res*boundary)
                    prey(i,j+3) = prey(i,j+3)-(prey(i,j)-res*boundary)*wallgain;
                elseif (prey(i,j)<res*(1-boundary))
                    prey(i,j+3) = prey(i,j+3)+(res*(1-boundary)-prey(i,j))*wallgain;
                end
            end
            
            % maintain distance between prey
            for j=1:preynum
                if (hypot(prey(i,x)-prey(j,x),hypot(prey(i,y)-prey(j,y),prey(i,z)-prey(j,z)))<preyspacing)
                    prey(i,xvel:zvel)=prey(i,xvel:zvel)+(prey(i,x:z)-prey(j,x:z))*spacinggain;
                end
            end
            
            % limit prey speed
            prey(i,xvel:zvel)=prey(i,xvel:zvel)*preyspeed/framerate/hypot(prey(i,xvel),hypot(prey(i,yvel),prey(i,zvel)));
        end
        
        prey(:,x:z)=prey(:,x:z)+prey(:,xvel:zvel);
        
        plot3(ceil(prey(:,x)),ceil(prey(:,y)),ceil(prey(:,z)),'. b','MarkerSize',preysize);
        axis(axes);
        view(azim,elev);
        azim=azim+rotspeed/framerate;
        writeVideo(movie,getframe);
    end
    close(movie);
end