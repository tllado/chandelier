function rain()
% raindrops falling

% User settings
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
numdrops=100;
speed=15;

elev=20;
rotspeed=15;
framerate=30;
movielength=900;
movietitle='../movies/rain.mp4';
movieformat='MPEG-4';
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    res=16;
    azim=0;
    x=1;
    y=2;
    z=3;
    axes=[1 res 1 res 1 res];
    movie = VideoWriter(movietitle,movieformat);
    open(movie);
    
    drops=ceil(res*rand(numdrops,3));
    
    for a=1:movielength
        drops(:,z)=drops(:,z)-speed/framerate;
        for i=1:numdrops
            if drops(i,z)==0
                drops(i,:)=[ceil(res*rand(x,y)) res];
            end
        end
        plot3(drops(:,x),drops(:,y),drops(:,z),'. ');
        axis(axes);
        view(azim,elev);
        azim=azim+rotspeed/framerate;
        writeVideo(movie,getframe);
    end
    close(movie);
end